﻿module Sql

type constant = 
    | ConstNull
    | ConstInt of int  
    | ConstMinusInt of int
    | ConstFloat of float  
    | ConstMinusFloat of float 
    | ConstString of string

type arythmOp = Plus | Minus | Time | Divise 

type op = Eq | Gt | Ge | Lt | Le | Lj | Rj | In | Ne | Is | Lk // =, >, >=, <, <=, *=, =*, IN, <>, IS, LIKE

type group<'content> = 
 | WithParenthesis of 'content
 | WithoutParenthesis of 'content

type callArg<'value> = 
   | Arg of 'value
   | OutArg of 'value

type callArgs<'value> = 
   | Any
   | Args of callArg<'value> list

type callStatement<'value> = {
    Name: string;
    Args: group<callArgs<'value>>;
    WithExecKeyword:bool;
}

type value<'subquery> =   
    | Null
    | Int of int  
    | MinusInt of int
    | Float of float  
    | MinusFloat of float  
    | String of string
    | Id of string
    | Call of callStatement<value<'subquery>>
    | Operation of value<'subquery> * arythmOp * value<'subquery>
    | SubQuery of 'subquery
 
type binaryBooleanOperator = And | Or
type unaryBooleanOperator = Not | Exists

type booleanExpression<'subquery> =
    | Cond of value<'subquery> * op * value<'subquery>
    | Exists of 'subquery
    | In of value<'subquery> * group<value<'subquery> list>
    | NotIn of value<'subquery> * group<value<'subquery> list>
    | IsNull of value<'subquery>
    | IsNotNull of value<'subquery>
    | Neg of group<booleanExpression<'subquery>>
    | BinaryCond of group<booleanExpression<'subquery>> * binaryBooleanOperator * group<booleanExpression<'subquery>>

type column<'subquery> = 
    | Column of value<'subquery> * value<'subquery> option
    | Set of string * value<'subquery>
    | ValueCase of string * (value<'subquery> * value<'subquery>) list * value<'subquery>
    | ExpressionCase of (group<booleanExpression<'subquery>> * value<'subquery>) list * value<'subquery>

type columns<'subquery> = 
   | ColumnList of column<'subquery> list
   | All

type table = string * string option
  
type ``type`` = 
           | SimpleType of string 
           | PreciseType of string * int 
           | VeryPreciseType of string * int * int


type parameter = {
    ParamName:string;
    Type:``type``;
    Default: constant option;
    IsOutput:bool;
}

type dir = Asc | Desc   
type order = string * dir

type joinType = Inner | Left | Right| Cross
 

type join<'subquery> = table * joinType * group<booleanExpression<'subquery>> option  

type selectQueryStatement =
      { Distinct : bool;
        Top : int option;       
        Tables : table list;   
        Columns : columns<selectQueryStatement>;   
        Joins : join<selectQueryStatement> list;   
        Where : group<booleanExpression<selectQueryStatement>> option;   
        OrderBy : order list;
        GroupBy : string list; }


type booleanExpression = booleanExpression<selectQueryStatement>
type join = join<selectQueryStatement>
type value = value<selectQueryStatement>
type callArgs = callArgs<value>

type variableDeclarationStatement = {
    VarName:string;
    Type:``type``;
    Default: value option;
}

type cursorOption =
   | ReadOnly
   | Update of string list option

type declareCursor = {
    CursorName:string;
    Query:selectQueryStatement;
    Options:cursorOption option
}

type set = string * value

type updateQueryStatement =
      { Table : table;   
        Set : set list;   
        Where : group<booleanExpression> option; }

type insertedValues = 
   | Select of selectQueryStatement
   | Values of value list

type insertQueryStatement =
      { Table : table;   
        Columns : string list;
        Values : insertedValues; }

type deleteQueryStatement =
      { Table : table;   
        Where : group<booleanExpression> option;
        Id : string option;
        Joins : join list;}

type cursorStatement =
    | Declare of declareCursor
    | Open of string
    | Fetch of string * value list
    | Close of string
    | Deallocate of string

type statement<'statementBloc> = 
    | SelectQueryStatement of selectQueryStatement
    | UpdateQueryStatement of updateQueryStatement
    | InsertQueryStatement of insertQueryStatement
    | DeleteQueryStatement of deleteQueryStatement
    | VariableDeclarationStatement of variableDeclarationStatement
    | RaiseErrorStatement of int * string
    | IfStatement of group<booleanExpression> * 'statementBloc * 'statementBloc option
    | WhileStatement of group<booleanExpression> * 'statementBloc
    | CallStatement of callStatement<value>
    | CursorStatement of cursorStatement
    | LabelStatement of string
    | GotoStatement of string
    | ReturnStatement of value option

type statementBloc =
    | SingleStatement of statement<statementBloc>
    | Bloc of statementBloc list

type statement = statement<statementBloc>

type createProcStatement = 
    {
        Name : string;
        Params : group<parameter list> option;
        Body : statementBloc;
        Options : string option;
    }

type goSeparatedStatement = 
    | StatementBloc of statementBloc
    | CreateProcedureStatement of createProcStatement


type script = {
    Statements: goSeparatedStatement list
}