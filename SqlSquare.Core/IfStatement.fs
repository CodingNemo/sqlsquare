﻿module internal IfStatement

open Sql

let private nextIndentLevel statementBloc currentIndentLevel =
    match statementBloc with
    | SingleStatement(_) -> currentIndentLevel + 1
    | Bloc(_) -> currentIndentLevel

let private transpileElseStatements elseStatements transpileStatementsBloc indentlevel =
    match elseStatements with 
    | None -> ""
    | Some(statements) -> 
        
        let indentation = Indent.tab indentlevel
        let elseContent = transpileStatementsBloc statements (nextIndentLevel statements indentlevel)
        sprintf "%s%sELSE%s%s" Indent.newline indentation Indent.newline elseContent

let transpile = 
    fun(transpileBooleanGroupExpression, transpileStatementsBloc) ->
        fun booleanExpression thenStatements elseStatements indentlevel ->
           let conditions = transpileBooleanGroupExpression booleanExpression indentlevel
           let thenClause = transpileStatementsBloc thenStatements (nextIndentLevel thenStatements indentlevel)
           let elseClause = transpileElseStatements elseStatements transpileStatementsBloc indentlevel
           sprintf "IF%s%s%s" conditions thenClause elseClause
