﻿module StoredProcedure

open Sql

let private transpileParamDefaultValue(def) =
   match def with 
   | None -> ""
   | Some(c) -> " = " + match c with 
                        | ConstNull -> "NULL"
                        | ConstInt(i) -> sprintf "%i" i
                        | ConstMinusInt(i) -> sprintf "-%i" i
                        | ConstFloat(f) -> sprintf "%g" f
                        | ConstMinusFloat(f) -> sprintf "-%g" f
                        | ConstString(s) -> s

let private transpileProcParam(param:parameter) =
    let indentation = Indent.tab 1
    let paramName = param.ParamName
    let paramType = Type.transpile(param.Type)
    let paramDefault = transpileParamDefaultValue(param.Default)
    let paramOutput = if param.IsOutput then " OUTPUT" else "";

    sprintf "%s%s %s%s%s" indentation paramName paramType paramDefault paramOutput

let rec private transpileProcParams(procParams) =
    let separator = "," + Indent.newline;
    let coreParams = procParams |> List.map(fun p -> transpileProcParam(p)) |> String.concat separator
    sprintf "%s%s%s" Indent.newline coreParams Indent.newline

let private transpileProcedureParameters = 
    fun transpileGroup -> 
        fun(procParams) ->
           match procParams with 
           | None -> ""
           | Some(paramsBloc) -> 
                let transpileParamsGroup = transpileProcParams |> transpileGroup
                transpileParamsGroup paramsBloc
 
let private transpileProcedureOptions(procOptions) =
   match procOptions with
   | None -> ""
   | Some(opt) -> 
        sprintf "WITH %s%s" opt Indent.newline

let transpile = 
    fun (transpileStatementsBloc, transpileGroup) ->        
        fun(createProcedure:createProcStatement) -> 
            let procName = createProcedure.Name
            let transpileParameters = transpileGroup |> transpileProcedureParameters
            let procParams = transpileParameters createProcedure.Params
            let procOptions =  transpileProcedureOptions createProcedure.Options
            let procBody = transpileStatementsBloc createProcedure.Body

            sprintf "CREATE PROC %s%s%s%sAS%s%s%sGO%s" procName procParams Indent.newline procOptions Indent.newline procBody Indent.newline Indent.newline