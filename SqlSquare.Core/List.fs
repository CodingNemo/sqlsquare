﻿module List

let split length (xs: seq<'T>) =
        let rec loop xs =
            [
                yield Seq.truncate length xs |> Seq.toList
                match Seq.length xs <= length with
                | false -> yield! loop (Seq.skip length xs)
                | true -> ()
            ]
        loop xs

let transpile groupSize indentlevel (seq: seq<'T>) =
    let separateByCommas = String.concat ", "

    let splitBySize = groupSize |> split 
    let groups = seq |> splitBySize

    if groups.Length > 1 
    then    
        let separator = "," + Indent.newline
        let indentation = Indent.tab indentlevel

        let lines = 
            groups 
                |> List.map(fun c -> separateByCommas(c)) 
                |> List.map(fun c -> indentation + c)
                |> String.concat separator
                           
        Indent.newline + lines
    else 
        groups.Head |> separateByCommas