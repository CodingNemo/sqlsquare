﻿module Indent

open System

let tab level =
    "    " |> String.replicate level

let line level =
    Environment.NewLine |> String.replicate level

let newline = line 1

//let flatten = fun(s:string) -> 
//    s.Replace(Environment.NewLine, "↲")
//     .Replace("\t", "↦")
//     .Trim()