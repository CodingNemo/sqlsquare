﻿module internal GroupBy

let rec private transpileColumns(groupBy) =
    match groupBy with
    | [] -> raise(Exceptions.Invalid("GroupBy must not be Empty")) // already handled by parser
    | [gb] -> gb
    | gb::tail -> gb + ", " + transpileColumns(tail)

let transpile groupBy indentLevel =
        match groupBy with
        | [] -> ""
        | _ as columns -> 
            let indentation = Indent.tab indentLevel
            let groupByContent = transpileColumns(columns)
            sprintf "%s%sGROUP BY %s" Indent.newline indentation groupByContent
