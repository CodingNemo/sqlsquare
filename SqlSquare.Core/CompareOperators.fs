﻿module internal CompareOperators

open Sql   

exception InvalidOperator of string

let transpile(op:op):string =
    match op with 
     | Eq -> " = "
     | Gt -> " > "
     | Ge -> " >= "
     | Lt -> " < "
     | Le -> " <= "
     | Lj -> raise(InvalidOperator("*= : should be replaced by a left join"))
     | Rj -> raise(InvalidOperator("=* : should be replaced by a right join"))
     | Is  -> " IS "
     | Lk -> " LIKE "
     | Ne -> " != "
