﻿module internal TransformSelectStatement

open Sql

open System
open System.Linq

let private prefix(value:value) : string =
    match value with 
    | Id(id) -> id.Split('.').FirstOrDefault()
    | _ -> ""
 
type private inserted = 
| Inserted
| NotInserted

type private joinNode(table:table, joinType:joinType option, conditions: group<booleanExpression> option, parent: joinNode option) as self =
    
    let mutable conditions: group<booleanExpression> option=conditions;
    let mutable children:joinNode list = [];
    let mutable joinType:joinType option = joinType;
    
    let childPosition = match parent with 
                      | None -> 0
                      | Some(p) -> p.Children.Length;

    member this.ChildPosition = childPosition
    member this.Children:joinNode list = children;
    member this.Table:table = table;
    member this.Conditions = conditions;
    member this.JoinType = joinType;
    member this.Parent:joinNode option = parent;
    member this.Depth = match parent with 
                           | None -> 0
                           | Some(p) -> p.Depth + 1;

    member this.IsRoot = match parent with 
                           | None -> true
                           | Some(p) -> false;
    
    member private this.findChild(table):joinNode option =
        let found = children |> Seq.filter (fun c -> c.Table = table) |> Seq.toList
        match found with 
        | [] -> None
        | [node] -> Some(node)
        | node::other -> failwith "This node can't have two children with the same table"

    member private this.mergeChildren(newChildren: joinNode list) =
        newChildren |> Seq.iter self.addChild 

    member private this.mergeConditions(newConditions) =
        match conditions with
        | None -> conditions <- newConditions
        | Some(c) -> match newConditions with
                     | None -> ()
                     | Some(nc) -> conditions <- Some(WithoutParenthesis(BinaryCond(c, And, nc)))

    member private this.mergeJoinType(newJoinType:joinType option) =
        match joinType with
        | None -> joinType <- newJoinType
        | Some(jt) ->( 
                        match newJoinType with 
                        | None -> ()
                        | Some(njt) -> match jt with 
                                      | Inner -> if [Left;Right;Cross].Contains(njt) then joinType <- newJoinType
                                      | Left -> if njt = Right then joinType <- Some(Cross)
                                      | Right -> if njt = Left then joinType <- Some(Cross)
                                      | Cross -> ()
                     )

    member private this.findTableInTree(table): joinNode option =
        if Table.areEqual(self.Table, table) then
            Some(self)
        else 
            try
                let found = self.Children |> Seq.pick (fun c -> c.findTableInTree(table))
                Some(found)
            with 
            | _ -> None 

    member this.containsTable(table) : bool = 
        let node = self.findTableInTree(table)
        match node with
        | None -> false
        | Some(_) -> true

    member this.mergeWith(node:joinNode) =

        if(not(joinType = node.JoinType)) then  
            self.mergeJoinType(node.JoinType)
        self.mergeConditions(node.Conditions)
        self.mergeChildren(node.Children)


    member this.addChild = fun(child:joinNode)->
         let matchingChild = self.findChild(child.Table)
         match matchingChild with
         | None -> children <- child::children
         | Some(c) -> c.mergeWith(child)

    member this.insert(left:table, joinType:joinType, right:table, conditions: group<booleanExpression>) : inserted =
        let leftFound = self.findTableInTree(left)
        let rightFound = self.findTableInTree(right)
        
        if leftFound.IsNone && rightFound.IsNone then 
            NotInserted 
        else 
            if leftFound.IsSome && rightFound.IsNone then
                let child = new joinNode(right, Some(joinType), Some(conditions), leftFound)
                leftFound.Value.addChild(child)
            elif leftFound.IsNone && rightFound.IsSome then
                let child = new joinNode(left, Some(joinType), Some(conditions), rightFound)
                rightFound.Value.addChild(child)
            else 
                let rightFoundValue = rightFound.Value
                let leftFoundValue = leftFound.Value

                if rightFoundValue.Depth > leftFoundValue.Depth then 
                    rightFoundValue.mergeWith(new joinNode(right, Some(joinType), Some(conditions), rightFoundValue.Parent)) 
                elif rightFoundValue.Depth < leftFoundValue.Depth then 
                    leftFoundValue.mergeWith(new joinNode(left, Some(joinType), Some(conditions), leftFoundValue.Parent)) 
                else 
                    if leftFoundValue.ChildPosition < rightFoundValue.ChildPosition then
                        leftFoundValue.mergeWith(new joinNode(left, Some(joinType), Some(conditions), leftFoundValue.Parent)) 
                    elif leftFoundValue.ChildPosition > rightFoundValue.ChildPosition then
                        rightFoundValue.mergeWith(new joinNode(right, Some(joinType), Some(conditions), rightFoundValue.Parent))
                    else 
                        failwith "Two child Nodes can't have the same Child Position"
            Inserted

    static member root(left:table, joinType:joinType, right:table, conditions: group<booleanExpression>) : joinNode =
        let root = new joinNode(left, None, None, None)
        root.addChild(new joinNode(right, Some(joinType), Some(conditions), Some(root)))
        root

let rec private createJoins(joinNodes:joinNode list) : join list =
    match joinNodes with 
    | [] -> []
    | [j] -> (j.Table, j.JoinType.Value, j.Conditions)::createJoins(j.Children)
    | j::tail -> List.append ((j.Table, j.JoinType.Value, j.Conditions)::createJoins(j.Children)) (createJoins(tail))

let private operatorToJoinType op =
    match op with 
    | Lj -> Left
    | Rj -> Right
    | _ -> Inner

let private replaceOperator(op) =
    match op with 
    | Lj -> Eq
    | Rj -> Eq
    | _ -> op

let private fillTreesWithJoin(join, trees:joinNode list) : joinNode list = 
     try
        trees |> List.find (fun t -> t.insert(join) = Inserted) |> ignore
        trees
     with 
     | :? System.Collections.Generic.KeyNotFoundException -> let root = join |> joinNode.root;
                                                             root::trees

let rec private fillTreesWithJoins(joins, trees:joinNode list) : joinNode list= 
     match joins with 
     | [] -> trees
     | [j] -> fillTreesWithJoin(j, trees)
     | j::tail -> let newTrees = fillTreesWithJoin(j, trees)     
                  fillTreesWithJoins(tail, newTrees)

let private buildJoinTrees(joins) : joinNode list = 
     fillTreesWithJoins(joins, [])

let rec private extractJoinsFromWhere = fun (conditions, tablesSymbolsTable:Map<string,table>) ->
     match Group.content(conditions) with 
     | Cond(left, op, right) -> if tablesSymbolsTable.ContainsKey(prefix(left)) && tablesSymbolsTable.ContainsKey(prefix(right)) 
                                then [(tablesSymbolsTable.Item(prefix(left)), operatorToJoinType(op), tablesSymbolsTable.Item(prefix(right)), Group.replaceContent(conditions, Cond(left, replaceOperator(op), right)))] 
                                else []
     | BinaryCond(left, _, right) -> List.append (extractJoinsFromWhere(left, tablesSymbolsTable)) (extractJoinsFromWhere(right, tablesSymbolsTable))
     | Neg group -> extractJoinsFromWhere(group, tablesSymbolsTable)
     | _ -> []

let rec private remainingWhereConditions = fun (conditions, tablesSymbolsTable:Map<string,table>) ->
     match Group.content(conditions) with 
     | Cond(left, op, right) -> if tablesSymbolsTable.ContainsKey(prefix(left)) && tablesSymbolsTable.ContainsKey(prefix(right)) 
                                then None 
                                else Some(conditions)
     | BinaryCond(left, op, right) -> 
                                let remainingleft = remainingWhereConditions(left, tablesSymbolsTable)
                                let remainingRight = remainingWhereConditions(right, tablesSymbolsTable)
                                if remainingleft.IsNone && remainingRight.IsSome then remainingRight
                                elif remainingleft.IsSome && remainingRight.IsNone then remainingleft
                                elif remainingleft.IsSome && remainingRight.IsSome then Some(Group.replaceContent(conditions, BinaryCond(remainingleft.Value, op, remainingRight.Value)))  
                                else None
     | Neg group -> let remainingGroup = remainingWhereConditions(group, tablesSymbolsTable)
                    if remainingGroup.IsSome then Some(conditions)
                    else None
     | _ -> Some(conditions)
    
let private extractJoinTree(selectStatement:selectQueryStatement) : joinNode list * group<booleanExpression> option =
    match selectStatement.Tables with 
    | [] -> ([], selectStatement.Where)
    | tables -> (
                    let tablesSymbolsTable = tables 
                                               |> Seq.groupBy (fun t-> Table.id(t))
                                               |> Seq.map (fun (k, values) -> (k, values.Single()))
                                               |> Map.ofSeq

                    let (joinsFromWhere, remainingWhereConditions) =
                        match selectStatement.Where with
                        | None -> ([], None)
                        | Some(conditions) ->  ((conditions, tablesSymbolsTable) |> extractJoinsFromWhere, (conditions, tablesSymbolsTable) |> remainingWhereConditions)
                    
                    let joinTrees = buildJoinTrees(joinsFromWhere)
                    (joinTrees, remainingWhereConditions)                     
                )

let rewriteIfNecessary(selectStatement:selectQueryStatement) : selectQueryStatement = 

    let (joinTrees, where) = selectStatement |> extractJoinTree

    let rootTables = joinTrees |> List.map (fun root -> root.Table)

    let lostTables = selectStatement.Tables |> List.filter (fun table -> not(joinTrees |> List.exists (fun tree -> tree.containsTable(table))))

    let joinsFromWhereClause = joinTrees |> List.collect (fun root -> root.Children) |> createJoins

    let allJoins = selectStatement.Joins |> List.append joinsFromWhereClause
    
    {
        Distinct= selectStatement.Distinct;
        Top= selectStatement.Top;
        Tables= rootTables |> List.append lostTables;
        Columns= selectStatement.Columns;
        Joins= allJoins;
        Where= where;
        OrderBy=selectStatement.OrderBy;
        GroupBy=selectStatement.GroupBy;
   }
