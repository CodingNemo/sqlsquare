﻿module internal Joins

open Sql

let private transpileOptionalOnClause = 
    fun(transpileBooleanGroupExpression:(group<booleanExpression> -> int -> string)) -> 
        fun indentLevel -> 
            fun optionalOn  ->
               match optionalOn with
               | None -> ""
               | Some(on) ->
                    let onClause = transpileBooleanGroupExpression on indentLevel
                    let indentation = Indent.tab indentLevel
                    sprintf "%s%sON%s" Indent.newline indentation onClause

let private transpileJoinType(joinType:joinType)  = 
   match joinType with 
   | Inner -> "INNER"
   | Left -> "LEFT"
   | Right -> "RIGHT"
   | Cross -> "ERROR" 

let private transpileOne = 
    fun (transpileBooleanGroupExpression:(group<booleanExpression> -> int -> string)) -> 
        fun indentLevel -> 
            fun join ->
                let (table, joinType, on) = join
                let indentation = Indent.tab indentLevel
                let transpiledTable = Table.transpileOne(table)

                match joinType with 
                | Cross -> 
                    sprintf "%s%sCROSS JOIN %s" Indent.newline indentation transpiledTable
                | _ ->     
                    let joinType = transpileJoinType(joinType) 
                    let nextIndentLevel = indentLevel + 1
                    let transpileOnClause = nextIndentLevel |> (transpileBooleanGroupExpression |> transpileOptionalOnClause)
                    let clauses = on |> transpileOnClause
                    sprintf "%s%s%s JOIN %s%s" Indent.newline indentation joinType transpiledTable clauses

let rec transpileMany = 
    fun transpileBooleanGroupExpression ->       
        fun joins indentLevel -> 
            let transpileJoin = indentLevel |> (transpileBooleanGroupExpression |> transpileOne)
            joins |> List.map transpileJoin |> String.concat ""
                