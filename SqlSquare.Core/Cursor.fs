﻿module internal Cursor

open Sql

let private transpileCursorOption cursorOption indentlevel =

    let forUpdateOption = fun cols ->
        match cols with 
        | None -> ""
        | Some(columnList) -> 
            let columns = columnList |> String.concat ", "
            sprintf " OF %s" columns

    let forOption = fun opt ->
        match opt with 
        | ReadOnly -> "READ ONLY"
        | Update(cols) -> 
            let updateOptions = forUpdateOption(cols)
            sprintf "UPDATE%s" updateOptions

    match cursorOption with 
    | None -> ""
    | Some(opt) -> 
        let identation = Indent.tab indentlevel
        let options = forOption(opt)
        sprintf "%s%sFOR %s" Indent.newline identation options

let private transpileCursorDeclaration =
    fun transpileSelectStatement -> 
        fun (cursor:declareCursor) indent -> 
            let nextindent = indent+1 
            let nextindentation = Indent.tab nextindent
            let select = transpileSelectStatement cursor.Query nextindent
            let options = transpileCursorOption cursor.Options indent
            sprintf "DECLARE %s CURSOR FOR%s%s%s%s" cursor.CursorName Indent.newline nextindentation select options

let transpile = 
    fun(transpileSelectStatement, transpileValue:value -> int -> string) -> 
        fun (cursorStatement:cursorStatement) indentlevel -> 
           match cursorStatement with 
           | Declare(declare) -> 
                let transpileDeclare = transpileSelectStatement |> transpileCursorDeclaration
                transpileDeclare declare indentlevel
           | Open(cursorName) -> sprintf "OPEN %s" cursorName
           | Fetch(cursorName, ids) -> 
                let transpileCursorColumns = transpileValue |> Value.transpileMany
                let cursorColumns = transpileCursorColumns ids indentlevel
                sprintf "FETCH %s INTO %s" cursorName cursorColumns
           | Close(cursorName) -> sprintf "CLOSE %s" cursorName
           | Deallocate(cursorName) -> sprintf "DEALLOCATE %s" cursorName