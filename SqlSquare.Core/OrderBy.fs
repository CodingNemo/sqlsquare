﻿module internal OrderBy

open Sql

let private transpileOrderByDirection(dir:dir) =
    match dir with 
    | Asc -> ""
    | Desc -> "DESC"

let rec private transpileOrderBy(orderBy) =
    match orderBy with 
    | [] -> raise(Exceptions.Invalid("OrderBy must not be Empty"))
    | [(id,dir)] -> id + " " + transpileOrderByDirection(dir)
    | (id,dir)::tail -> id + " " + transpileOrderByDirection(dir) + "," + transpileOrderBy(tail)


let transpile orderBy indentLevel =
    match orderBy with 
    | [] -> ""
    | o -> 
            let indentation = Indent.tab indentLevel
            let orderByContent = transpileOrderBy(o)
            sprintf "%s%sORDER BY %s" Indent.newline indentation orderByContent
