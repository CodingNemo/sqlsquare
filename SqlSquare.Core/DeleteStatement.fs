﻿module internal DeleteStatement

open Sql

let private transpileOptionalId(id) =
    match id with 
    | None -> ""
    | Some(i) -> i + " "

let transpile = 
    fun (transpileTable, transpileJoins, transpileWhere) -> 
        fun(deleteStatement:deleteQueryStatement) indent ->

            let where = transpileWhere deleteStatement.Where indent
            let joins = transpileJoins deleteStatement.Joins indent
            let id = transpileOptionalId deleteStatement.Id 
            let table = transpileTable deleteStatement.Table

            sprintf "DELETE %sFROM %s%s%s" id table joins where