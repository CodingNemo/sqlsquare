﻿module QueryStatement

open Sql

let private transpileRaiseError error indentLevel =
    sprintf "RAISERROR(%s,1,1)" error

let private transpileLabel label indentLevel =
    sprintf "%s%s: %s" Indent.newline label Indent.newline

let private transpileGoto label indentLevel =
    sprintf "GOTO %s" label

let transpile =    
    fun (transpileStatementsBlocPartial:(statement<statementBloc> -> int -> string) -> (statementBloc -> int -> string)) -> 
        let rec transpileQueryStatement statement  =
            let transpileBooleanExpressionGroupPartial = 
                fun (selectQueryStatement:selectQueryStatement -> int -> string) -> 
                    fun (transpileValue:value -> int -> string) -> 
                        let transpileContent = (selectQueryStatement, transpileValue) |> BooleanExpression.transpile
                        let rec transpileContentGroup content i = Group.transpile(fun c -> transpileContent(c, i))(content)
                        transpileContentGroup

            let transpileValuePartial = Group.transpile |> Value.transpileOne

            let transpileSelectStatement = SelectStatement.transpile(transpileValuePartial, transpileBooleanExpressionGroupPartial)   
            let transpileValue = transpileSelectStatement |> transpileValuePartial

            let transpileBooleanExpressionGroup =  transpileValue |> (transpileSelectStatement |> transpileBooleanExpressionGroupPartial)

            let transpileStatementsBloc = transpileQueryStatement |> transpileStatementsBlocPartial

            let transpileWhere = transpileBooleanExpressionGroup |> Where.transpile
            let transpileJoins = transpileBooleanExpressionGroup |> Joins.transpileMany

            match statement with
            | SelectQueryStatement(select) -> transpileSelectStatement select 
            | InsertQueryStatement(insert) -> 
                let transpileInsertStatement = (Table.transpileOne, transpileValue, transpileSelectStatement) |> InsertStatement.transpile
                transpileInsertStatement insert
            | DeleteQueryStatement(delete) -> 
                let transpileDeleteStatement = (Table.transpileOne, transpileJoins, transpileWhere) |> DeleteStatement.transpile
                transpileDeleteStatement delete 
            | UpdateQueryStatement(update) -> 
                let transpileUpdateStatement = (Table.transpileOne, transpileValue, transpileWhere) |> UpdateStatement.transpile
                transpileUpdateStatement update 
            | VariableDeclarationStatement(declare) -> 
                let transpileDeclarationStatement = transpileValue |> DeclarationStatement.transpile
                transpileDeclarationStatement declare 
            | RaiseErrorStatement(_, error) -> 
                transpileRaiseError error
            | IfStatement(conditions, thenStatements, elseStatements) -> 
                let transpileIfStatement = (transpileBooleanExpressionGroup, transpileStatementsBloc) |> IfStatement.transpile
                transpileIfStatement conditions thenStatements elseStatements
            | WhileStatement(conditions, statements) -> 
                let transpileWhileStatement = (transpileBooleanExpressionGroup, transpileStatementsBloc) |> WhileStatement.transpile
                transpileWhileStatement conditions statements
            | CallStatement(call) -> 
                let transpileCallStatement = (transpileValue, Group.transpile) |> CallStatement.transpile
                transpileCallStatement call
            | CursorStatement(cursor) -> 
                let transpileCursor = (transpileSelectStatement, transpileValue) |> Cursor.transpile
                transpileCursor cursor
            | LabelStatement(label) -> 
                transpileLabel label
            | GotoStatement(label) -> 
                transpileGoto label
            | ReturnStatement(value) -> 
                let transpileReturn = transpileValue |> Return.transpile
                transpileReturn value
            | _ -> raise(Exceptions.NotSupportedYet)
        transpileQueryStatement