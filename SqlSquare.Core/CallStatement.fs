﻿module internal CallStatement

open Sql

let private transpileCallArg = 
    fun (transpileValue) ->
        fun arg indentlevel->
            match arg with 
            | Arg(a) -> transpileValue a indentlevel
            | OutArg(a) -> transpileValue a indentlevel + " OUTPUT"
            
let private transpileAnyCallArgs = 
    fun(transpileValue) ->
        fun args indentlevel ->
            match args with 
            | Any -> "*"
            | Args(argsList) -> 
                let transpileArg = transpileValue |> transpileCallArg
                argsList |> List.map (fun a -> transpileArg a indentlevel) |> String.concat ","

let transpile = 
    fun(transpileValue, transpileGroup) ->
        fun (call:callStatement<value>) indentlevel ->
            let transpileAllCallArgs = transpileValue |> transpileAnyCallArgs
            let transpileGroupOfArgs = (fun args -> transpileAllCallArgs args indentlevel) |> transpileGroup

            let callName = if call.Name = "char_length" then "LEN" else call.Name
            let exec = if call.WithExecKeyword then "EXEC " else ""
            let args = transpileGroupOfArgs call.Args
            exec + callName + args