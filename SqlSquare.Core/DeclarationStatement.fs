﻿module internal DeclarationStatement

open Sql
open System

let private transpileDefault(d, transpileValue) =
    match d with 
    | None -> ""
    | Some(def) -> " = " + transpileValue(def)

let transpile = 
    fun(transpileValue) -> 
        fun(declarationStatement:variableDeclarationStatement) indent ->
           let varType = declarationStatement.Type |> Type.transpile 
           let defValue = transpileDefault(declarationStatement.Default, fun v -> transpileValue v indent)
           "DECLARE " + declarationStatement.VarName + " " + varType + defValue

