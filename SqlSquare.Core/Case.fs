﻿module internal CaseWhen

open Sql

let private transpileWhenValueClause =
    fun transpileValue -> 
        fun (whenClause:value*value) -> 
            let (w, t) = whenClause
            let whenValue = transpileValue w
            let thenValue = transpileValue t
            sprintf "WHEN %s THEN %s" whenValue thenValue

let rec private transpileWhenValueClauses = 
    fun (transpileValue:value -> string) -> 
        fun (whenClauses:(value*value) list) ->
            let transpileOneWhenValueClause = transpileValue |> transpileWhenValueClause
            whenClauses |> List.map transpileOneWhenValueClause
                        |> String.concat " "

let private transpileWhenExpressionClause = 
    fun(transpileValue:value -> string, transpileBooleanExpressionGroup:group<booleanExpression> -> string) -> 
        fun(whenClause) ->
            let (w, t) = whenClause
            let whenExpression = transpileBooleanExpressionGroup w
            let thenValue = transpileValue(t)
            sprintf "WHEN %s THEN %s" whenExpression thenValue

let rec private transpileWhenExpressionClauses = 
    fun(transpileValue:value -> string, transpileBooleanExpressionGroup:group<booleanExpression> -> string) -> 
        fun whenClauses ->
            let transpileWhenExpressionClause = (transpileValue, transpileBooleanExpressionGroup) |> transpileWhenExpressionClause
            whenClauses |> List.map(fun whenClause -> transpileWhenExpressionClause(whenClause)) |> String.concat " " 

let transpileWhenId = 
    fun(transpileValue:value->int->string) -> 
        fun id (whenValue:(value*value) list) elseValue indent ->

            let indentedTranspileValue = fun v -> transpileValue v indent
            let transpileWhenValueClauses = indentedTranspileValue |> transpileWhenValueClauses

            let whenClause = transpileWhenValueClauses whenValue
            let elseClause = indentedTranspileValue elseValue

            sprintf "CASE %s %s ELSE %s END" id whenClause elseClause

let transpileWhenExpression =
    fun (transpileBooleanExpressionGroup, transpileValue) -> 
        fun whenExpression elseValue indentlevel -> 
            let transpileValueWithIndentation v = transpileValue v indentlevel
            let transpileBooleanExpressionGroupWithIndentation exp = transpileBooleanExpressionGroup exp indentlevel

            let transpileWhenExpressionClauses = (transpileValueWithIndentation,transpileBooleanExpressionGroupWithIndentation) |> transpileWhenExpressionClauses

            let whenExpressionClause = transpileWhenExpressionClauses whenExpression

            let elseClause = transpileValueWithIndentation elseValue
            sprintf "CASE %s ELSE %s END" whenExpressionClause elseClause

