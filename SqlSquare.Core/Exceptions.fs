﻿module Exceptions

exception Invalid of string
exception ParsingFailed of string
exception NotSupportedYet