﻿module internal Type

open Sql

let transpile t =
    match t with 
    | SimpleType(s) -> s
    | PreciseType(s,p) -> s + "(" + (if s = "varchar" && p > 8000 then "8000" else string(p)) + ")"
    | VeryPreciseType(s,p1,p2) -> s + "(" + string(p1) + "," + string(p2) + ")"
