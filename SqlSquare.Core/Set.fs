﻿module internal Set

let transpile = 
    fun transpileValue ->
        fun set indent ->
            let (id, value) = set
            id + " = " + transpileValue value indent