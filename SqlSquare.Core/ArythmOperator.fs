﻿module internal ArythmOperator

open Sql   

let transpile(op:arythmOp):string =
    match op with 
     | Plus -> " + "
     | Minus -> " - "
     | Time -> " * "
     | Divise -> " / "


