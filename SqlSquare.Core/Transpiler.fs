﻿module Transpiler

open Sql

open Microsoft.FSharp.Text
open System.IO

let private transpileStatementBlocs(blocs, transpileStatementsBloc) =
    blocs |> List.map(fun b -> transpileStatementsBloc(b)) |> String.concat Indent.newline

let rec private transpileStatementsBloc = 
    fun transpileStatement -> 
        fun (statementBloc:statementBloc) indentlevel ->
                let transpileStatementBlocWithQuery = transpileStatement |> transpileStatementsBloc
                let indentation = indentlevel |> Indent.tab  ;
                match statementBloc with 
                | SingleStatement(statement) -> Indent.newline + indentation + transpileStatement statement indentlevel
                | Bloc(blocs) -> 
                let transpiledStratementBlocContent = transpileStatementBlocs(blocs, fun b -> transpileStatementBlocWithQuery b (indentlevel + 1))
                Indent.newline + indentation + "BEGIN" + transpiledStratementBlocContent + Indent.newline + indentation + "END"
        
    
let private transpileGoSeparatedStatement(statement) =

    let transpileQueryStatement = transpileStatementsBloc |> QueryStatement.transpile

    let transpileRootStatementBloc bloc = (transpileQueryStatement |> transpileStatementsBloc) bloc 0

    match statement with
    | StatementBloc(bloc) -> transpileRootStatementBloc bloc
    | CreateProcedureStatement(p) -> 
        let transpileStoreProcedureStatement = (transpileRootStatementBloc, Group.transpile) |> StoredProcedure.transpile;
        transpileStoreProcedureStatement p

let rec private transpileGoSeparatedStatements(statements) =
    let separator = sprintf "%sGO%s" Indent.newline Indent.newline
    statements |> List.map transpileGoSeparatedStatement |> String.concat separator

let private writeAbstractSyntaxTree (ast:script) = 
     transpileGoSeparatedStatements(ast.Statements)

let private parse(lexbuf) : script =
    try
      SqlParser.start SqlLexer.tokenize lexbuf
    with e ->
      let pos = lexbuf.EndPos
      let line = pos.Line
      let column = pos.Column
      let message = e.Message
      let lastToken = new System.String(lexbuf.Lexeme)
      failwithf "Failure : %s. l.%d c.%d. Last Token %s." message line column lastToken

let fromString sql = 
   let lex = sql |> Lexing.LexBuffer<_>.FromString 
   let ast = lex |> parse 
   ast |> writeAbstractSyntaxTree

let fromFile(filePath:string)= 
   (new StreamReader(filePath)) |> Lexing.LexBuffer<_>.FromTextReader |> parse |> writeAbstractSyntaxTree