﻿module internal Table

open Sql

let private transpileTableId(tableId) =
    match tableId with 
    | None -> ""
    | Some(id) -> " " + id

let transpileOne(tableName, tableId) =
    tableName + transpileTableId(tableId)

let rec transpileMany(tables:table list) =
    match tables with
    | [(tableName, tableId)] -> transpileOne(tableName, tableId)
    | (tableName, tableId)::tail -> transpileOne(tableName, tableId) + ", " + transpileMany(tail)

let id(table:table) = 
    let (tableName, tableId) = table
    match tableId with 
    | None -> tableName
    | Some(id) -> id

let areEqual(t1,t2) = id(t1) = id(t2)