﻿module internal InsertStatement

open Sql

let private transpileInsertValues =
    fun(transpileSelectStatement, transpileValue) -> 
        fun insertedValues indentlevel ->
            let indentation = Indent.tab indentlevel
            let transpileValueIndented v = transpileValue v indentlevel

            match insertedValues with
            | Values(values) -> 
                            let transpiledValuesList = values |> List.map transpileValueIndented
                            let transpiledValues = transpiledValuesList |> List.transpile 7 (indentlevel + 1)
                            sprintf "%s%sVALUES(%s)" Indent.newline indentation transpiledValues
            | Select(selectStatement) -> 
                            let nextIndentationLevel = indentlevel+1
                            let nextIndentation = Indent.tab nextIndentationLevel
                            let selectStatement = transpileSelectStatement selectStatement nextIndentationLevel
                            sprintf "%s%s%s" Indent.newline nextIndentation selectStatement
   
let private transpileInsertColumns = 
    fun columns indentlevel-> 
        match columns with 
        | [] -> ""
        | _ as columnsList -> 
                let columns = columnsList |> List.transpile 7 indentlevel
                sprintf "(%s)" columns

let transpile = 
    fun (transpileTable, transpileValue, transpileSelectStatement) -> 
        fun (insertStatement:insertQueryStatement) indentlevel ->
            let table = transpileTable(insertStatement.Table)

            let insertsColumns = transpileInsertColumns insertStatement.Columns (indentlevel + 1)

            let transpileInsertValue = ((transpileSelectStatement, transpileValue) |> transpileInsertValues)
            let insertsValues = transpileInsertValue insertStatement.Values indentlevel

            sprintf "INSERT INTO %s%s%s" table insertsColumns insertsValues
