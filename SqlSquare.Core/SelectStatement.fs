﻿module internal SelectStatement

open Sql


        
let private transpileOptionalTop(top) =
    match top with 
    | None -> ""
    | Some(i) -> sprintf "TOP %i " i

let private tanspileOptionalDistinct(distinct) =
    if distinct then "DISTINCT " else ""

let private transpileFrom = 
        fun tables indentLevel ->
           match tables with 
           | [] -> ""
           | t  -> 
                let indentation = Indent.tab indentLevel
                let tables = Table.transpileMany(t)
                sprintf "%s%sFROM %s" Indent.newline indentation tables

let transpile = 
    fun(transpileValueFunctor:(selectQueryStatement -> int -> string) -> (value -> int -> string),
        transpileBooleanGroupExpressionFunctor:(selectQueryStatement -> int -> string) -> (value -> int -> string) -> (group<booleanExpression> -> int -> string)) -> 
            let rec internalTranspile selectStatement indentLevel = 
                let selectStatementToTranspile = selectStatement |> TransformSelectStatement.rewriteIfNecessary  

                let transpileValue = internalTranspile |> transpileValueFunctor                
                let transpileBooleanExpression = transpileBooleanGroupExpressionFunctor(internalTranspile)(transpileValue)

                let top = transpileOptionalTop selectStatementToTranspile.Top
                let distinct = tanspileOptionalDistinct selectStatementToTranspile.Distinct
                        
                let transpileColumns = (transpileValue |> (transpileBooleanExpression |> SelectColumns.transpile))
                let columns = transpileColumns selectStatementToTranspile.Columns (indentLevel + 1)

                let from = transpileFrom selectStatementToTranspile.Tables indentLevel

                let transpileJoins = transpileBooleanExpression |> Joins.transpileMany
                let joins = transpileJoins selectStatementToTranspile.Joins indentLevel

                let transpileWhere = transpileBooleanExpression |> Where.transpile
                let where = transpileWhere selectStatementToTranspile.Where indentLevel

                let groupyBy = GroupBy.transpile selectStatementToTranspile.GroupBy indentLevel
                let orderBy = OrderBy.transpile selectStatementToTranspile.OrderBy indentLevel

                sprintf "SELECT %s%s%s%s%s%s%s%s" top distinct columns from joins where groupyBy orderBy
            internalTranspile

