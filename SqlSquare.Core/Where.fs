﻿module internal Where

let transpile =
    fun (transpileBooleanGroupExpression:'a -> int -> string) ->
        fun where indentLevel ->
            match where with 
            | None -> ""
            | Some(expression) -> 
                let indentation = Indent.tab indentLevel

                let conditions = transpileBooleanGroupExpression expression indentLevel
                sprintf "%s%sWHERE%s" Indent.newline indentation conditions
