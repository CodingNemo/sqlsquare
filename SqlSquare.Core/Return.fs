﻿module internal Return

let transpile = 
    fun transpileValue -> 
        fun value indentlevel -> 
           match value with 
           | None -> "RETURN"
           | Some(v) -> 
                let transpiledValue = transpileValue v indentlevel
                sprintf "RETURN %s" transpiledValue