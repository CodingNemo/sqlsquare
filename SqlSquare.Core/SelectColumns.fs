﻿module SelectColumns

open Sql

let private transpileColumn = 
    fun(transpileValue) -> 
        fun name expression indent ->
            let left = transpileValue expression indent
            let right = 
                match name with
                | None  -> ""
                | Some(n) -> " AS " + transpileValue n indent
            left + right; 

let private transpileSelectColumn = 
    fun (transpileValue, transpileBooleanGroupExpression:(group<booleanExpression> -> int -> string)) -> 
        fun (column, indentlevel) ->
        match column with
        | Column(expression, name) -> ( transpileValue |> transpileColumn) name expression indentlevel
        | Set(id, value) -> (transpileValue |> Set.transpile) (id,value) indentlevel
        | ValueCase(id, whenClauses, elseClause) -> 
            let transpileCaseWhenId = transpileValue |> CaseWhen.transpileWhenId
            transpileCaseWhenId id whenClauses elseClause indentlevel
        | ExpressionCase(whenClauses, elseClause) -> 
            let transpileCaseWhenExpression = (transpileBooleanGroupExpression, transpileValue) |> CaseWhen.transpileWhenExpression
            transpileCaseWhenExpression whenClauses elseClause indentlevel
        
let transpile = 
    fun (transpileBooleanExpressionGroup) -> 
        fun transpileValue ->
            fun columns indentLevel ->
                match columns with 
                | All -> "*"
                | ColumnList(columnList) -> 
                    let transpileOneColumn = (transpileValue, transpileBooleanExpressionGroup) |> transpileSelectColumn 
                    let transpileOneColumnIndented c = transpileOneColumn(c, indentLevel)
                    let transpiledColumnsList = columnList |> List.map transpileOneColumnIndented

                    transpiledColumnsList |> List.transpile 7 indentLevel
                    
        
