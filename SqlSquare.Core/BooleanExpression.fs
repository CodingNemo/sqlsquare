﻿module internal BooleanExpression

open Sql

let private transpileBinaryBooleanOperator(op) =
   match op with
   | And -> "AND"
   | Or -> "OR"
   
let rec transpile = 
    fun(transpileSubQuery, transpileValue:value -> int -> string) ->
        fun(conditions:booleanExpression, indentLevel) ->

            let transpileConditions c = ((transpileSubQuery, transpileValue) |> transpile)(c, indentLevel)
            let transpileConditionsGroup = transpileConditions |> Group.transpile
                    
            let transpileValueWithIndent v = transpileValue v indentLevel

            let transpileManyValues = transpileValue |> Value.transpileMany
            let transpileManyValuesGroup = (fun v -> transpileManyValues v indentLevel) |> Group.transpile

            match conditions with
            | Cond(left, op, right) -> 
                let transpiledLeft = transpileValueWithIndent left
                let transpiledRight = transpileValueWithIndent right
                let compareOperator = CompareOperators.transpile op
                sprintf "%s%s%s" transpiledLeft compareOperator transpiledRight 
            | In(left, right) -> 
                let transpiledLeft = transpileValueWithIndent(left)
                let transpiledright = transpileManyValuesGroup(right)
                sprintf "%s IN %s" transpiledLeft transpiledright
            | NotIn(left, right) -> 
                let transpiledLeft = transpileValueWithIndent(left)
                let transpiledright = transpileManyValuesGroup(right)
                sprintf "%s NOT IN %s" transpiledLeft transpiledright
            | IsNull(left) -> 
                let transpiledLeft = transpileValueWithIndent(left)
                sprintf "%s IS NULL" transpiledLeft
            | IsNotNull(left) -> 
                let transpiledLeft = transpileValueWithIndent(left)
                sprintf "%s IS NOT NULL" transpiledLeft
            | Exists(subquery) -> 
                let nextindent = (indentLevel + 1)
                let nextIndentation = Indent.tab nextindent
                let subquery = transpileSubQuery subquery nextindent
                sprintf "EXISTS(%s%s%s)" Indent.newline nextIndentation subquery
            | Neg(conditions) ->
                let notConditions = transpileConditionsGroup(conditions)
                sprintf "NOT %s" notConditions
            | BinaryCond(left, op, right) -> 
                let indentation = Indent.tab (indentLevel + 1)
                let split = if op = And then Indent.newline + indentation else ""
                let transpiledLeftConditionGroup = transpileConditionsGroup(left)
                let transpiledRightConditionGroup = transpileConditionsGroup(right)
                let booleanOperator = transpileBinaryBooleanOperator(op)
                sprintf "%s%s%s%s" transpiledLeftConditionGroup split booleanOperator transpiledRightConditionGroup