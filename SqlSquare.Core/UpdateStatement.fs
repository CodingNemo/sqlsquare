﻿module internal UpdateStatement

open Sql

let transpile = 
    fun (transpileTable, transpileValue:value -> int -> string, transpileWhere) ->
        fun (updateStatement:updateQueryStatement) indentlevel ->

            let where = transpileWhere updateStatement.Where indentlevel

            let transpileSet = transpileValue |> Set.transpile
            let sets = updateStatement.Set |> List.map (fun s -> transpileSet s indentlevel)

            let indentation = Indent.tab indentlevel

            let transpiledSets = sets |> List.transpile 1 (indentlevel + 1)
           
            let table = transpileTable(updateStatement.Table) + Indent.newline  + indentation

            sprintf "UPDATE %sSET %s%s" table transpiledSets where