﻿module internal Group

open Sql

let private trim(content:string):string = 
    content.Trim(' ')

let transpile = 
    fun compileContent ->
        fun(group) ->
           match group with 
           | WithParenthesis(content) -> "(" + trim(compileContent(content)) + ")"
           | WithoutParenthesis(content) -> " " + trim(compileContent(content)) + " "

let replaceContent(group, newContent) =
   match group with 
   | WithParenthesis(_) -> WithParenthesis(newContent)
   | WithoutParenthesis(_) -> WithoutParenthesis(newContent)

let content group =
   match group with 
   | WithParenthesis(content) -> content
   | WithoutParenthesis(content) -> content