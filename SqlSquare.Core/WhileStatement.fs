﻿module internal WhileStatement

let transpile = 
    fun (transpileBooleanExpressionGroup, transpileStatementsBloc) ->
        fun  conditions statements indentlevel ->
            let whileConditions = transpileBooleanExpressionGroup conditions indentlevel
            let whileContent = transpileStatementsBloc statements indentlevel
            sprintf "WHILE%s%s" whileConditions whileContent