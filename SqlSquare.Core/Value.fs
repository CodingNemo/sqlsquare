﻿module internal Value

open Sql

let private transformReservedId(id:string) =
    match id with 
    | "@@sqlstatus" -> "@@FETCH_STATUS"
    | _ -> id

let transpileOne = 
    fun transpileGroup ->
        fun transpileSubQuery ->
            let rec transpileValue = fun value indent -> 
                    match value with 
                    | Null -> "NULL"
                    | Int(v) -> sprintf "%i" v
                    | MinusInt(v) -> sprintf "-%i" v
                    | Float(v) -> sprintf "%g" v
                    | MinusFloat(v) -> sprintf "-%g" v
                    | String(v) -> 
                        let valueWithoutQuotes = v.Substring(1, v.Length - 2)
                        sprintf "'%s'" valueWithoutQuotes
                    | Id(v) -> transformReservedId(v)
                    | Call(call) -> 
                        let transpileCall = (transpileValue, transpileGroup) |> CallStatement.transpile
                        transpileCall call indent
                    | Operation(vLeft,op,vRight) -> transpileValue vLeft indent + ArythmOperator.transpile(op) + transpileValue vRight indent
                    | SubQuery(query) -> 
                        let nextIndent = indent + 1
                        let nextIndentation = Indent.tab nextIndent
                        let subQuery = transpileSubQuery query nextIndent
                        sprintf "(%s%s%s)" Indent.newline nextIndentation subQuery
            transpileValue

let rec transpileMany =
    fun (transpileValue:value -> int -> string) ->
        fun values indent -> 
            values |> List.map (fun s -> transpileValue s indent)
                   |> String.concat ", "