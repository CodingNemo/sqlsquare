﻿using System;
using System.IO;
using System.Linq;
using Anotar.NLog;
using CommandLine;
using SqlSquare.Exceptions;
using SqlSquare.Input;

namespace SqlSquare
{
    class Program
    {
        [LogToFatalOnException]
        static void Main(string[] args)
        {
            var parseResult = Parser.Default.ParseArguments<Argument>(args);
            if (parseResult.Errors.Any())
            {
                WaitForAKeyToBePressed();
                return;
            }

            var argument = CheckArguments(parseResult);

            var sybaseConnectionString = argument.SourceConnectionString;

            var source = new Database.Database(sybaseConnectionString, argument.SourceProviderName ?? "Sybase.Data.AseClient");

            var objectsToExport = DbObjectParser.FromInputFile(argument.InputFile, source);

            var script = ScriptGenerator.GenerateScript(objectsToExport, source);

            ScriptExporter.ExportScriptToFile(script, argument.OutputFolder, argument.OutputFileName);

            LogTo.Info("GENERATION DONE");

            WaitForAKeyToBePressed();
        }

        private static void WaitForAKeyToBePressed()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;

            Console.WriteLine("-- Please type any key --");
            Console.ReadKey();
        }

        private static Argument CheckArguments(ParserResult<Argument> parseResult)
        {
            var argument = parseResult.Value;
            if (!File.Exists(argument.InputFile))
            {
                throw new InvalidInputException($"Could not find File {argument.InputFile}.");
            }

            return argument;
        }
    }
}
