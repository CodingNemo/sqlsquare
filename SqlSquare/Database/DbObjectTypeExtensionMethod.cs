﻿using SqlSquare.Exceptions;

namespace SqlSquare.Database
{
    public static class DbObjectTypeExtensionMethod
    {
        public static DbObjectType ToDbObjectType(this string objectTypeAsString)
        {
            var trimmedObjectTypeAsString = objectTypeAsString.Trim();

            switch (trimmedObjectTypeAsString)
            {
                case "U":
                case "user table":
                    return DbObjectType.Table;
                case "P":
                case "stored procedure":
                    return DbObjectType.StoredProcedure;
                default:
                    throw new UnhandledObjectType($"Object of type {trimmedObjectTypeAsString} can not be processed.");
            }
        }
    }
}