﻿using System;
using System.Data;
using System.Data.Common;

namespace SqlSquare.Database
{
    public class Database
    {
        private readonly string _connectionString;
        private readonly string _providerName;

        public Database(
            string connectionString,
            string providerName)
        {
            _connectionString = connectionString;
            _providerName = providerName;
        }

        private string _name;

        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    var connectionStringBuilder = new DbConnectionStringBuilder
                    {
                        ConnectionString = _connectionString
                    };

                    _name = connectionStringBuilder["Database"]?.ToString();
                }
                return _name;
            }
        }

        public IDbConnection GetConnection()
        {
            var dbProviderFactory = DbProviderFactories.GetFactory(_providerName);
            var dbConnection = dbProviderFactory.CreateConnection();

            if (dbConnection == null)
            {
                throw new Exception("NO CONNECTION");
            }

            dbConnection.ConnectionString = _connectionString;
            return dbConnection;
        }

        public override string ToString()
        {
            return $"Name:{Name}; Provider:{_providerName}; ConnectionString:{_connectionString};";
        }
    }
}