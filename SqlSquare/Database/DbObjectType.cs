﻿namespace SqlSquare.Database
{
    public enum DbObjectType
    {
        Table,
        StoredProcedure
    }
}