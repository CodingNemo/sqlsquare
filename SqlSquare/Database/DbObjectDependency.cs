﻿namespace SqlSquare.Database
{
    public struct DbObjectDependency
    {
        public string Type { get; set; }
        public string Object { get; set; }
    }
}