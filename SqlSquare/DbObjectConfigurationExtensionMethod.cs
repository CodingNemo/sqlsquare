﻿using System;

namespace SqlSquare
{
    public static class DbObjectConfigurationExtensionMethod
    {
        public static bool Excluded(this string conf)
        {
            return string.Equals(conf, "excluded", StringComparison.OrdinalIgnoreCase);
        }

        public static bool WithScript(this string conf)
        {
            return conf?.StartsWith("postscript", StringComparison.OrdinalIgnoreCase) ?? false;
        }

        public static string ExtractScript(this string conf)
        {
            return Extract(conf, DbObject.NoScript);
        }

        public static bool WithContent(this string conf)
        {
            return conf?.StartsWith("content", StringComparison.OrdinalIgnoreCase) ?? false;
        }

        public static string ExtractContentFilter(this string conf)
        {
            return Extract(conf, DbObject.NoFilter);
        }

        private static string Extract(string conf, string defaultConf)
        {
            if (string.IsNullOrEmpty(conf))
            {
                return null;
            }

            var filterStart = conf.IndexOf('(');

            if (filterStart < 0)
            {
                return defaultConf;
            }

            return conf.Substring(filterStart + 1)
                .Replace("(", "")
                .Replace(")", "");
        }
    }
}