using System.Collections.Generic;

namespace SqlSquare.Scripts
{
    public class ScriptParams
    {
        private readonly ScriptParams _parent;
        public Dictionary<string, int> WidthPerField = new Dictionary<string, int>();
        private readonly Dictionary<string, object> _values = new Dictionary<string, object>();

        public string FormatField(string field, string fieldValue)
        {
            return fieldValue.PadRight(WidthPerField[field]);
        }

        public ScriptParams()
            : this(null)
        {
        }

        public ScriptParams(ScriptParams parent)
        {
            _parent = parent;
        }

        public ScriptParams CreateChild()
        {
            return new ScriptParams(this);
        }

        public bool Exists(string key)
        {
            return _values.ContainsKey(key);
        }

        public void Add(string key, object value)
        {
            _values.Add(key, value);
        }

        public object Get(string key)
        {
            if (_parent != null && _parent.Exists(key))
            {
                return _parent.Get(key);
            }

            return _values[key];
        }
    }
}