﻿using System.Text;

namespace SqlSquare.Scripts
{
    public class HeaderScript : Script
    {
        private readonly string _databaseName;

        public HeaderScript(string databaseName)
        {
            _databaseName = databaseName;
        }

        public string ObjectName => _databaseName;

        public string ToScript(ScriptParams @params)
        {
            var script = new StringBuilder();
            script.Append(DropIfDbExists());
            script.AppendLine();
            script.AppendLine($"CREATE DATABASE {_databaseName}");
            script.AppendLine("GO");
            script.AppendLine($"USE {_databaseName}");
            script.AppendLine("GO");

            return script.ToString();
        }

        private string DropIfDbExists()
        {
            return $@"
IF (EXISTS (SELECT name 
FROM master.dbo.sysdatabases 
WHERE ('[' + name + ']' = '{_databaseName}'
OR name = '{_databaseName}')))
BEGIN
    PRINT 'DROPING DATABASE {_databaseName}'
    DROP DATABASE {_databaseName}
END
GO";
        }
    }
}
