using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;

namespace SqlSquare.Scripts
{
    internal class ColumnScript : Script
    {
        public static IEnumerable<ColumnScript> Query(IDbConnection connection, string tableName)
        {
            return connection.Query<ColumnScript>(
                @"SELECT DISTINCT col.name AS [name],   
                       colType.name + CASE WHEN colType.name like '%char%' THEN '(' + CONVERT(varchar, col.length) + ')' 
                                      ELSE CASE col.prec WHEN NULL THEN '' ELSE '(' + CONVERT(varchar, col.prec) + ',' + CONVERT(varchar, col.scale) + ')' END END AS [type],
                       CASE col.status WHEN 0x80 THEN 1 ELSE 0 END AS [identity],
                       CASE WHEN convert(bit, (col.status & 8)) <> 0 THEN 1 ELSE 0 END AS [nullable],
	                   CASE col.cdefault WHEN 0 THEN NULL ELSE (SELECT text FROM syscomments WHERE id = col.cdefault) END AS [default]
                FROM dbo.syscolumns col
                INNER JOIN dbo.sysobjects tab
                    ON col.id = tab.id
                INNER JOIN dbo.systypes colType
                    ON col.usertype = colType.usertype
                WHERE tab.name IN @ObjectNames", 
                new { ObjectNames = new[] { tableName, WithoutSchema(tableName) } });
        }

        private static string WithoutSchema(string tableName)
        {
            return tableName.Split('.').Last();
        }

        public string Name { get; private set; }
        public string Type { get; private set; }

        public bool Identity { get; set; }

        public string IdentityLabel => Identity ? "IDENTITY" : string.Empty;
        public string Default { get; set; }

        public bool Nullable { get; set; }

        public string NullableLabel => Nullable ? "NULL" : "NOT NULL";

        public string ObjectName { get; }

        public string ToScript(ScriptParams @params)
        {
            return $"   {@params.FormatField(nameof(Name), Name)} {@params.FormatField(nameof(Type), Type)} {@params.FormatField(nameof(IdentityLabel), IdentityLabel)} {@params.FormatField(nameof(Default), Default?.Trim().Replace("\"", "'") ?? string.Empty)} {@params.FormatField(nameof(NullableLabel), NullableLabel.Trim())}";
        }
    }
}