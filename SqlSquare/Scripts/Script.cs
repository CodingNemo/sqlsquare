namespace SqlSquare.Scripts
{
    public interface Script
    {   
        string ObjectName { get; }

        string ToScript(ScriptParams @params);
    }
}