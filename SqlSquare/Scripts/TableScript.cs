using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlSquare.Scripts
{
    internal class TableScript : Script, IEquatable<TableScript>
    {
        private readonly List<ColumnScript> _columns;
        private readonly List<ConstraintScript> _constraints;
        private readonly List<IndexScript> _indexes;
        private readonly string _script;
        private readonly List<InsertScript> _inserts;
        private string _primaryKeyName;

        public string ObjectName { get; }

        public TableScript(string tableName, List<ColumnScript> columns, List<ConstraintScript> constraints, List<IndexScript> indexes, string script, List<InsertScript> inserts = null)
        {
            ObjectName = tableName;
            _columns = columns;
            _constraints = constraints;
            _indexes = indexes;
            _script = script;
            _inserts = inserts;
        }

        public string ToScript(ScriptParams @params)
        {
            var tableScriptParams = @params.CreateChild();
            tableScriptParams.Add("Table", ObjectName);

            var scriptBuilder = new StringBuilder();

            scriptBuilder.AppendLine($"PRINT 'TABLE {ObjectName}'");
            scriptBuilder.AppendLine("GO");

            scriptBuilder.AppendLine(DropIfTableExists());

            this.AppendPrefixTo(scriptBuilder);

            ColumnsToScript(tableScriptParams, scriptBuilder);

            ConstraintsToScript(tableScriptParams, scriptBuilder);

            IndexesToScript(tableScriptParams, scriptBuilder);

            InsertRowToScript(tableScriptParams, scriptBuilder);

            if (!string.IsNullOrEmpty(_script))
            {
                scriptBuilder.AppendLine();
                scriptBuilder.AppendLine(_script);
                scriptBuilder.AppendLine();
            }

            return scriptBuilder.ToString();
        }

        private string DropIfTableExists()
        {
            return $@"
IF OBJECT_ID('{ObjectName}', 'U') IS NOT NULL
BEGIN
    PRINT 'DROPING TABLE {ObjectName}'
    DROP TABLE {ObjectName}
END
GO";
        }

        private void InsertRowToScript(ScriptParams tableScriptParams, StringBuilder scriptBuilder)
        {
            if (_inserts == null || !_inserts.Any())
            {
                return;
            }

            scriptBuilder.AppendLine($"PRINT 'TABLE {ObjectName} - INSERTS'");
            scriptBuilder.AppendLine("GO");

            var insertScriptsParams = tableScriptParams.CreateChild();

            _inserts.ForEach(i => scriptBuilder.AppendLine(i.ToScript(insertScriptsParams)));

        }

        private void ConstraintsToScript(ScriptParams @params, StringBuilder scriptBuilder)
        {
            var notAlreadyScriptedConstraints =
                _constraints.Where(
                    c =>
                        !c.Definition.StartsWith("DEFAULT") &&
                        !c.Definition.Contains("FOREIGN KEY") &&
                        !string.Equals(c.Name.Trim(), _primaryKeyName?.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();

            if (!notAlreadyScriptedConstraints.Any())
            {
                return;
            }

            var constraintsScriptParams = @params.CreateChild();
            constraintsScriptParams.WidthPerField = new Dictionary<string, int>
            {
                { "Name", notAlreadyScriptedConstraints.Max(c => c.Name.Length)},
                { "Definition", notAlreadyScriptedConstraints.Max(c => c.Definition.Length)},
            };

            foreach (var constraint in notAlreadyScriptedConstraints)
            {
                scriptBuilder.AppendLine(constraint.ToScript(constraintsScriptParams));
            }
            scriptBuilder.AppendLine("GO");
        }

        private void ColumnsToScript(ScriptParams @params, StringBuilder scriptBuilder)
        {
            scriptBuilder.AppendLine($"CREATE TABLE {ObjectName} (");

            var columnScriptParams = @params.CreateChild();

            columnScriptParams.WidthPerField = new Dictionary<string, int>
            {
                {"Name", _columns.Max(c => c.Name.Length)},
                {"Type", _columns.Max(c => c.Type.Length)},
                {"Default", _columns.Max(c => c.Default?.Length ?? 0)},
                {"IdentityLabel", _columns.Max(c => c.IdentityLabel.Length)},
                {"NullableLabel", _columns.Max(c => c.NullableLabel.Length)}
            };
            
            var currentLine = 1;

            foreach (var column in _columns)
            {
                var columnScript = column.ToScript(columnScriptParams);

                if (currentLine < _columns.Count)
                {
                    columnScript += ",";
                }

                scriptBuilder.AppendLine(columnScript);

                currentLine++;
            }

            PrimaryKey(scriptBuilder);

            scriptBuilder.AppendLine(")");
            scriptBuilder.AppendLine("GO");
        }

        private void PrimaryKey(StringBuilder scriptBuilder)
        {
            var primaryKey = _constraints.FirstOrDefault(c => c.Definition.Contains("PRIMARY KEY"));

            if (primaryKey == null)
            {
                return;
            }
            _primaryKeyName = primaryKey.Name;

            var clustered = primaryKey.Definition.Contains("CLUSTERED") ? "CLUSTERED" : string.Empty;
            var firstBracket = primaryKey.Definition.IndexOf('(');
            var lastBracket = primaryKey.Definition.IndexOf(')');
            var keys = primaryKey.Definition.Substring(firstBracket + 1, lastBracket - firstBracket - 1)
                    .Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(k => k.Trim())
                    .ToList();
            scriptBuilder.Append(",");
            scriptBuilder.AppendLine();

            scriptBuilder.AppendLine($"PRIMARY KEY {clustered} (");
            var keyIndex = 1;

            foreach (var key in keys)
            {
                var keyScript = $"   {key}";
                if (keyIndex < keys.Count)
                {
                    keyScript += ",";
                }
                scriptBuilder.AppendLine(keyScript);
                keyIndex++;
            }

            scriptBuilder.AppendLine(")");
        }

        private void IndexesToScript(ScriptParams @params, StringBuilder scriptBuilder)
        {
            if (!_indexes.Any())
            {
                return;
            }

            var indexScriptParams = @params.CreateChild();
            indexScriptParams.WidthPerField = new Dictionary<string, int>
            {
                {"Index_Name", _indexes.Max(c => c.Index_Name.Length)},
                {"Description", _indexes.Max(c => c.Description.Length)},
                {"Index_Keys", _indexes.Max(c => c.Index_Keys.Length)},
            };

            foreach (var index in _indexes.Where(i => !string.Equals(i.Index_Name.Trim(), _primaryKeyName?.Trim(), StringComparison.OrdinalIgnoreCase)))
            {
                scriptBuilder.AppendLine(index.ToScript(indexScriptParams));
            }

            scriptBuilder.AppendLine("GO");
        }

        public bool Equals(TableScript other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(ObjectName, other.ObjectName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableScript)obj);
        }

        public override int GetHashCode()
        {
            return (ObjectName != null ? ObjectName.GetHashCode() : 0);
        }

        public static bool operator ==(TableScript left, TableScript right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TableScript left, TableScript right)
        {
            return !Equals(left, right);
        }
    }
}