﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using Dapper;
using SqlSquare.Exceptions;

namespace SqlSquare.Scripts
{
    internal class StoredProcedureScript : Script
    {
        private readonly List<string> _textLines;

        public static IEnumerable<string> QueryText(IDbConnection connection, string procedureName)
        {
            var result = connection.QueryMultiple("sp_helptext @ObjectName", new {ObjectName = procedureName});

            var numberOfLine = result.ReadSingle<int>();

            return result.Read<string>();
        }

        public StoredProcedureScript(string procedureName, List<string> textLines)
        {
            _textLines = textLines;
            ObjectName = procedureName;
        }

        public string ObjectName { get; }

        public string ToScript(ScriptParams @params)
        {
            var scriptBuilder = new StringBuilder();
            
            _textLines.ForEach(line => scriptBuilder.Append(line));

            scriptBuilder.AppendLine("GO");
            var script = scriptBuilder.ToString();
            var transpiled = Transpile(script);

            var stringBuilderWithPrefix = new StringBuilder();

            stringBuilderWithPrefix.AppendLine($"PRINT 'STORED PROCEDURE {ObjectName}'");
            stringBuilderWithPrefix.AppendLine("GO");
            stringBuilderWithPrefix.AppendLine(DropIfStoredProcedureExists());
            this.AppendPrefixTo(stringBuilderWithPrefix);
            stringBuilderWithPrefix.AppendLine(transpiled);

            return stringBuilderWithPrefix.ToString();
        }

        private string DropIfStoredProcedureExists()
        {
            return $@"
IF OBJECT_ID('{ObjectName}', 'P') IS NOT NULL
BEGIN
    PRINT 'DROPING STORED PROCEDURE {ObjectName}'
    DROP PROC {ObjectName}
END
GO";
        }


        private string Transpile(string script)
        {
            try
            {
                return Transpiler.fromString(script);
            }
            catch (Exception ex)
            {
                throw new TranspilationException($"Could not transpile stored procedure {ObjectName}", ex);
            }
        }
    }
}