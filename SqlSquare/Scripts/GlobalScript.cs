﻿using System;

namespace SqlSquare.Scripts
{
    public class GlobalScript : Script
    {
        public string ObjectName => $"DDL_Script_from_{_header.ObjectName}";

        private readonly HeaderScript _header;
        private readonly Script _content;

        public GlobalScript(HeaderScript header, Script content)
        {
            _header = header;
            _content = content;
        }

        public string ToScript(ScriptParams @params)
        {
            var headerScript = _header.ToScript(@params);

            var contentScript = _content.ToScript(@params);

            return headerScript + Environment.NewLine + contentScript;
        }
    }
}
