using System.Text;

namespace SqlSquare.Scripts
{
    public static class ScriptExtensionMethods
    {
        public static void AppendPrefixTo(this Script script, StringBuilder scriptBuilder)
        {
            var commentWithoutName = $"/****************{new string('*', script.ObjectName.Length)}*****/";
            var comment = $"/*   SCRIPTS FOR {script.ObjectName}    */";

            scriptBuilder.AppendLine(commentWithoutName);
            scriptBuilder.AppendLine(comment);
            scriptBuilder.AppendLine(commentWithoutName);
        }
    }
}
