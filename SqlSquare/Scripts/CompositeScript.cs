﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SqlSquare.Scripts
{
    public class CompositeScript : Script
    {
        public string ObjectName => string.Join(" - ", _scripts.Select(s => s.ObjectName));

        private readonly List<Script> _scripts;

        private static readonly string Separator = "GO" + Environment.NewLine + Environment.NewLine;

        public CompositeScript(params Script[] scripts)
        {
            _scripts = scripts.ToList();
        }

        public CompositeScript(IEnumerable<Script> scripts)
        {
            _scripts = scripts.ToList();
        }

        public string ToScript(ScriptParams @params)
        {
            return string.Join(Separator, _scripts.Select(_ => _.ToScript(@params)).ToList());
        }
    }
}
