using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;

namespace SqlSquare.Scripts
{
    internal class IndexScript : Script
    {
        public static IEnumerable<IndexScript> Query(IDbConnection connection, string tableName)
        {
            return connection.Query<IndexScript>("sp_helpindex @ObjectName", new { ObjectName = tableName });
        }

        public string Index_Description { get; set; }

        public string Description => string.Join(" ", Index_Description.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(_ => _.Trim()).OrderByDescending(_ => _)).ToUpperInvariant();

        public string Index_Name { get; set; }
        public string Index_Keys { get; set; }

        public string ObjectName { get; }

        public string ToScript(ScriptParams @params)
        {
            return $"CREATE {@params.FormatField(nameof(Description), Description)} INDEX {@params.FormatField(nameof(Index_Name), Index_Name)} ON {@params.Get("Table")} ({@params.FormatField(nameof(Index_Keys), Index_Keys)})";
        }
    }
}