using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;

namespace SqlSquare.Scripts
{
    internal class InsertScript : Script
    {
        private readonly Dictionary<string, object> _values;

        public static IEnumerable<InsertScript> Query(IDbConnection connection, string objectName, string filter)
        {
            var query = "select * from " + objectName;

            if (filter != DbObject.NoFilter)
            {
                var trimedFilter = filter.Trim();

                if (!trimedFilter.StartsWith("where", StringComparison.OrdinalIgnoreCase))
                {
                    trimedFilter = "where " + trimedFilter;
                }

                query += " " + trimedFilter;
            }

            var rows = connection.Query(query);

            foreach (var row in rows)
            {
                var values = new Dictionary<string, object>();

                foreach (var data in row)
                {
                    values.Add(data.Key.ToString(), data.Value);
                }
                yield return new InsertScript(values);
            }
        }

        public InsertScript(Dictionary<string, object> values)
        {
            _values = values;
        }

        public string ObjectName { get; } = string.Empty;
        public string ToScript(ScriptParams @params)
        {
            return $"INSERT INTO {@params.Get("Table")} ({string.Join(",", _values.Keys)}){Environment.NewLine}VALUES ({string.Join(",", _values.Values.Select(ToString))})";
        }

        private static string ToString(object val)
        {
            if (val is string || val is char)
            {
                return $"'{val.ToString().Replace("'", "''")}'";
            }

            if (val is DateTime)
            {
                return $"'{(DateTime)val:yyyyMMdd hh:mm:ss.fff tt}'";
            }

            if (val is bool)
            {
                return ((bool)val) ? "1" : "0";
            }

            if (val == null)
            {
                return "NULL";
            }

            return val.ToString();
        }
    }
}