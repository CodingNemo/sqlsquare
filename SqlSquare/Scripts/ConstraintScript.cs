using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Dapper;

namespace SqlSquare.Scripts
{
    internal class ConstraintScript : Script
    {
        public static IEnumerable<ConstraintScript> Query(IDbConnection connection, string tableName)
        {
            try
            {
                return connection.Query<ConstraintScript>("sp_helpconstraint @ObjectName", new {ObjectName = tableName});
            }
            catch
            {
                return Enumerable.Empty<ConstraintScript>();
            }
        }

        public string Name { get; set; }

        public string Definition { get; set; }

        public string ObjectName { get; }

        public string ToScript(ScriptParams @params)
        {
            return $"ALTER TABLE {@params.Get("Table")} ADD {@params.FormatField(nameof(Definition), Definition)}";
        }
    }
}