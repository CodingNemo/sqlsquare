﻿using System;
using SqlSquare.Database;

namespace SqlSquare
{
    public class DbObject
    {
        public const string NoFilter = "*";
        public const string NoScript = null;

        public static DbObject New(string fullName, string content, bool excluded, string script, DbObjectType type)
        {
            return new DbObject(fullName, content, excluded, script, type);
        }

        public static DbObject Dependency(string fullName, DbObjectType type)
        {
            return new DbObject(fullName, NoFilter, false, NoScript, type);
        }

        private DbObject(
            string fullName,
            string content,
            bool excluded,
            string script, 
            DbObjectType type)
        {
            var splittedFullName = fullName.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
                
            Name = splittedFullName.Length > 1 ? splittedFullName[1] : splittedFullName[0];
            Owner = splittedFullName.Length > 1 ? splittedFullName[0] : default(string);

            ContentFilter = content;
            Script = script;
            Excluded = excluded;
            Type = type;
        }


        public string Owner { get; }

        public string Name { get; }

        public bool WithContent => !string.IsNullOrEmpty(ContentFilter);

        public string ContentFilter { get; }

        public string Script { get; }
        public bool WithScript => !string.IsNullOrEmpty(Script);

        public bool Excluded { get; }

        public DbObjectType Type { get; }

        public override string ToString()
        {
            var result = $"{Name}:{Type}";

            if (WithContent)
            {
                result += " - WithContent : " + ContentFilter;
            }

            if (WithScript)
            {
                result += " - WithScript : " + Script;
            }

            if (Excluded)
            {
                result += " - Excluded";
            }

            return result;
        }
    }
}