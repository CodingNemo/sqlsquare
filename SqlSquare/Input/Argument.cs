﻿using CommandLine;

namespace SqlSquare.Input
{
    internal class Argument
    {
        [Option('i', "input", Required = true,
            HelpText = "Input file containing definition of sql objects to extract.")]
        public string InputFile { get; set; }

        [Option('f', "folder", Required = false,
            HelpText = "Output folder to store generated sql. Default : Local folder.")]
        public string OutputFolder { get; set; }

        [Option('o', "output", Required = false,
            HelpText = "Output file name sql. Default : ddMMMyyyy_Generate_DDL_script.sql")]
        public string OutputFileName { get; set; }

        [Option('s', "source", Required = true, 
            HelpText = "Connection string of the Source Database. ")]
        public string SourceConnectionString { get; set; }

        [Option('p', "sourceProvider", Required = false,
            HelpText = "Provider Name of the Source Database. Default : Sybase.Data.AseClient")]
        public string SourceProviderName { get; set; }

        //[Option('t', "target", Required = false,
        //    HelpText = "Connection string of the Target Database on which the script should be run.")]
        //public string TargetConnectionString { get; set; }

        //[Option('q', "targetProvider", Required = false,
        //    HelpText = "Provider Name of the Target Database. Default : System.Data.SqlClient")]
        //public string TargetProviderName { get; set; }

    }
}