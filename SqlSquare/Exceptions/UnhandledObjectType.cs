﻿using System;
using System.Runtime.Serialization;

namespace SqlSquare.Exceptions
{
    [Serializable]
    internal class UnhandledObjectType : Exception
    {
        public UnhandledObjectType()
        {
        }

        public UnhandledObjectType(string message) : base(message)
        {
        }

        public UnhandledObjectType(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnhandledObjectType(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}