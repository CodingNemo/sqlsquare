﻿using System;
using System.IO;
using Anotar.NLog;
using Dapper;
using SqlSquare.Scripts;

namespace SqlSquare
{
    internal static class ScriptExporter
    {
        public static void ExportScriptToFile(Script script, string folder, string fileName)
        {
            LogTo.Info("Exporting script to file");

            folder = folder ?? string.Empty;

            if (!string.IsNullOrEmpty(folder) && !Directory.Exists(folder))
            {
                LogTo.Debug($"Creating directory {folder}");
                Directory.CreateDirectory(folder);
            }

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = $"{DateTime.Now:ddMMMyy}_{script.ObjectName}.sql";
                LogTo.Debug($"No input file name specified. Using {fileName}");
            }
            else if (!fileName.EndsWith(".sql"))
            {
                fileName += ".sql";
            }

            var path = Path.Combine(folder, fileName);

            if (File.Exists(path))
            {
                LogTo.Debug($"File {path} already exists. Deleting existing file.");
                File.Delete(path);
            }

            LogTo.Info($"Writing script '{script.ObjectName}' to {path}");
            File.WriteAllText(path, script.ToScript(new ScriptParams()));
        }

        public static void ExportScriptToDatabase(Script script, Database.Database target)
        {
            LogTo.Info($"Exporting script to Database");

            using (var connection = target.GetConnection())
            {
                connection.Open();

                var transaction = connection.BeginTransaction();
                try
                {
                    connection.Execute(script.ToScript(new ScriptParams()), transaction: transaction);
                    transaction.Commit();
                    LogTo.Info("Exporting script to Database Done");
                }
                catch (Exception ex)
                {
                    LogTo.ErrorException("Exporting script to Database failed", ex);
                    transaction.Rollback();
                }
            }
        }
    }
}