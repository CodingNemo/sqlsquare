﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Anotar.NLog;
using Dapper;
using SqlSquare.Database;
using SqlSquare;

namespace SqlSquare
{
    public static class DbObjectParser
    {
        public static IEnumerable<DbObject> FromInputFile(string inputFile, Database.Database source)
        {
            LogTo.Info($"Parse db object names to export from file {inputFile}");

            var fileLines =
                File.ReadAllLines(inputFile)
                    .Where(l => !l.TrimStart().StartsWith("--"))
                    .Where(l => !string.IsNullOrEmpty(l));

            var dbObjects = new List<DbObject>();

            using (var connection = source.GetConnection())
            {
                foreach (var line in fileLines)
                {
                    var splittedLine = line.Trim().Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                    var withContent = splittedLine.SingleOrDefault(_ => _.WithContent())
                                                 ?.ExtractContentFilter();

                    var excluded = splittedLine.Any(_ => _.Excluded());

                    var script = splittedLine.SingleOrDefault(_ => _.WithScript())
                                            ?.ExtractScript();

                    var objectName = splittedLine.Single(_ => !_.WithContent() && !_.Excluded() && !_.WithScript());

                    var type = GetObjectType(connection, objectName);
                    dbObjects.Add(DbObject.New(objectName, withContent, excluded, script, type));
                }
            }

            LogTo.Debug($"Found {dbObjects.Count} objects.");
            return dbObjects;
        }

        private static DbObjectType GetObjectType(IDbConnection dbConnection, string objectName)
        {
            var objectType = dbConnection.QueryFirst<string>("select type from dbo.sysobjects where name = @ObjectName", new { ObjectName = objectName });
            return objectType.ToDbObjectType();
        }
    }
}