﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Anotar.NLog;
using Dapper;
using SqlSquare.Database;
using SqlSquare.Scripts;

namespace SqlSquare
{
    public static class ScriptGenerator
    {
        private static readonly Dictionary<DbObjectType, Func<IDbConnection, DbObject, List<string>, Script>> Generators =
            new Dictionary<DbObjectType, Func<IDbConnection, DbObject, List<string>, Script>>()
            {
                [DbObjectType.StoredProcedure] = GenerateStoredProcedureScript,
                [DbObjectType.Table] = GenerateTableScript

            };

        public static Script GenerateScript(IEnumerable<DbObject> objectsToExport, Database.Database source)
        {
            LogTo.Info("Generating script");
            LogTo.Debug($"Source : {source}");

            using (var connection = source.GetConnection())
            {
                var globalScript = new GlobalScript(
                    new HeaderScript(source.Name),
                    GenerateScripts(objectsToExport, connection, new List<string>()));

                LogTo.Info($"Script {globalScript.ObjectName} generation DONE");
                return globalScript;
            }
        }

        private static Script GenerateScripts(IEnumerable<DbObject> objectsToExport, IDbConnection connection, List<string> alreadyExported)
        {
            var scripts = new List<Script>();

            foreach (var obj in objectsToExport.Where(_ => !_.Excluded))
            {
                if (alreadyExported.Contains(obj.Name))
                {
                    LogTo.Trace($"Script {obj} already generated");
                    continue;
                }

                var generator = Generators[obj.Type];
                LogTo.Debug($"Generating script for {obj}");

                var script = generator(connection, obj, alreadyExported);
                scripts.Add(script);

                alreadyExported.Add(obj.Name);
            }

            if (scripts.Count == 1)
            {
                return scripts.Single();
            }

            return new CompositeScript(scripts);
        }

        private static Script GenerateTableScript(IDbConnection dbConnection, DbObject dbObject, List<string> alreadyExported)
        {
            var tableName = dbObject.Name;

            LogTo.Debug($"TABLE SCRIPT : {tableName}");

            var columns = ColumnScript.Query(dbConnection, tableName).ToList();
            var indexes = IndexScript.Query(dbConnection, tableName).ToList();
            var constraints = ConstraintScript.Query(dbConnection, tableName).ToList();

            if (dbObject.WithContent)
            {
                LogTo.Debug($"INSERT INTO TABLE SCRIPT : {tableName}");
                var inserts = InsertScript.Query(dbConnection, tableName, dbObject.ContentFilter).ToList();
                return new TableScript(tableName, columns, constraints, indexes, dbObject.Script, inserts);
            }

            return new TableScript(tableName, columns, constraints, indexes, dbObject.Script);
        }

        private static Script GenerateStoredProcedureScript(IDbConnection dbConnection, DbObject dbObject, List<string> alreadyExported)
        {
            var procedureName = dbObject.Name;

            LogTo.Debug($"STORED PROCEDURE SCRIPT : {procedureName}");

            var dependenciesScripts = GenerateDependenciesScripts(dbConnection, procedureName, alreadyExported);

            var lines = StoredProcedureScript.QueryText(dbConnection, procedureName).ToList();

            var storedProcedureScript = new StoredProcedureScript(procedureName, lines);

            if (dependenciesScripts != null)
            {
                LogTo.Debug($"STORED PROCEDURE SCRIPT WITH DEPENDENCIES : {procedureName}");
                return new CompositeScript(dependenciesScripts, storedProcedureScript);
            }

            return storedProcedureScript;
        }

        private static Script GenerateDependenciesScripts(IDbConnection dbConnection, string procedureName, List<string> alreadyExported)
        {
            LogTo.Debug("LOADING DEPENDENCIES");

            var dependencies = dbConnection.Query<DbObjectDependency>("sp_depends @ObjectName", new { ObjectName = procedureName }).ToList();

            LogTo.Trace(() => $"Found {dependencies.Count} dependencies");

            var dependenciesDbObject = dependencies.Select(d => DbObject.Dependency(d.Object, d.Type.ToDbObjectType()));

            return GenerateScripts(dependenciesDbObject, dbConnection, alreadyExported);
        }
    }
}
