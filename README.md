﻿# Sql²

Ansi Sql -> T-Sql

This tool is aimed to generate SQL Server object creation script to dupplicate the structure of a Sybase Database.

## Usage

To know the expected arguments, use :

> SqlSquare.exe --help

### Example 

> SqlSquare.exe proc_insert_table1, proc_update_table2 C:\Scripts\output.sql

The previous exemple will generate a CREATE script for stored procedurues *proc_insert_table1*, *proc_update_table2* and their **dependencies** like *table1*, *table2*

N.B. : The tool does not take into account dependencies from other schema such as audit..table1 yet

## Conversion from Sybase SQL To SQL Server SQL

Sybase supports both ANSI SQL and Transact SQL
SQL Server supports mostly TSQL. 
But it causes a compatibility issue. For instance **SQL Server does not take into account \*= or =\* joins** defined in ANSI SQL standard.

The Sql² Transpiler was created for that. (And to have fun with F#, of course)

### SqlSquare

This tools written in F# uses the [FsLex,FsYacc][fslexyacc] framework to write an ANSI SQL Parser and generate an AST.

For most of the SQL Code, we simply rewrite is as is. But in the following cases (fully described in [SqlSquare.Core.Tests.TranspilerShould][transpilershould]) we had to make changes.

#### Joins

But in the **specific case of SELECT statements** we rewrite them so they comply with Transac SQL joins. 

> SELECT * FROM t1, t2, t3 WHERE t1.id =* t2.id AND t3.id *= t2.id

... becomes ...

> SELECT * FROM t1 RIGHT JOIN t2 ON t1.id = t2.id LEFT JOIN t3 ON t3.id = t2.id

This is tricky and the main reason we had to parse the sql and generate an AST. This is not just a replace using regex. We have to make sure each join is well generated in the right order with the right ON conditions. We can't afford to have this :

> SELECT * FROM t1 LEFT JOIN t3 ON t3.id = t2.id RIGHT JOIN t2 ON t1.id = t2.id

This is still syntaxically valid TSQL but will fail at execution

#### varchar limit size

varchar precision is reduced to *8000* in SQL Server

#### Some other changes

| Sybase | SQL Server |
| ---------------------------------------| ------------------------------------- |
| char_length('text')                    | len('text')                           |
| RaisError 99999 'insertion impossible' | RAISERROR('insertion impossible',1,1) |
| @@sqlstatus                            | @@FETCH_STATUS                        |

I __don't garanty__ that this exe will transpile any ansi sql to tsql.

[fslexyacc]:http://fsprojects.github.io/FsLexYacc/
[transpilershould]:./SqlSquare.Core.Tests/TranspilerShould.fs