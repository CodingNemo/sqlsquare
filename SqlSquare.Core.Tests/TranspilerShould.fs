﻿module CompilerShould

open NUnit.Framework
open FsUnit

[<Test>]
let ``Should compile a simple SELECT statement as is`` () = 
    "select column1 as 'Alias', column2 AS COL from Table1 t1 where t1.id = 1 order by t1.date desc" 
    |> Transpiler.fromString
    |> should equal "
SELECT column1 AS 'Alias', column2 AS COL
FROM Table1 t1
WHERE t1.id = 1 
ORDER BY t1.date DESC"

[<Test>]
let ``Should compile a simple SELECT statement with many CONDITIONS in WHERE clause as is`` () = 
    "select * from Table1 t1 where t1.id = 1 and (t1.date <= getdate() or t1.date is NULL)" 
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table1 t1
WHERE t1.id = 1 
    AND(t1.date <= getdate() OR t1.date IS NULL) "

[<Test>]
let ``Should compile a simple SELECT statement with a negative CONDITION in WHERE clause as is`` () = 
    "select * from Table1 t1 where t1.id = 1 and not (t1.date <= getdate() or t1.date is not null)" 
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table1 t1
WHERE t1.id = 1 
    AND NOT (t1.date <= getdate() OR t1.date IS NOT NULL) "

[<Test>]
let ``Should compile a simple SELECT statement with a GroupBy as is`` () = 
    "select t1.id, t1.label, count(*) as \"COUNT\" from Table1 t1 group by t1.id, t1.label" 
    |> Transpiler.fromString
    |> should equal "
SELECT t1.id, t1.label, count(*) AS 'COUNT'
FROM Table1 t1
GROUP BY t1.id, t1.label"


[<Test>]
let ``Should compile a simple SELECT statement with a lot of columns grouping column by set of 7 per line`` () = 
    "select t1.col1, t1.col2, t1.col3, t1.col4, t1.col5, t1.col6, t1.col7, t1.col8, t1.col9, t1.col10, t1.col11, t1.col12, t1.col13, t1.col14, t1.col15 from Table1 t1" 
    |> Transpiler.fromString
    |> should equal "
SELECT 
    t1.col1, t1.col2, t1.col3, t1.col4, t1.col5, t1.col6, t1.col7,
    t1.col8, t1.col9, t1.col10, t1.col11, t1.col12, t1.col13, t1.col14,
    t1.col15
FROM Table1 t1"

[<Test>]
let ``Should compile a SELECT statement with an INNER JOIN as is`` () = 
    "select * from Table1 t1 inner join Table2 t2 on t1.id = t2.id group by t1.id" 
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table1 t1
INNER JOIN Table2 t2
    ON t1.id = t2.id 
GROUP BY t1.id"

[<Test>]
let ``Should compile a SELECT statement with an INNER JOIN and many CONDITIONS as is`` () = 
    "Select * from Table1 t1 inner join Table2 t2 on t1.id = t2.id and (t1.date = getdate() Or t1.date = t2.date) group by t1.id" 
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table1 t1
INNER JOIN Table2 t2
    ON t1.id = t2.id 
        AND(t1.date = getdate() OR t1.date = t2.date) 
GROUP BY t1.id"

[<Test>]
let ``Should compile a SELECT statement with a TOP clause as is`` () = 
    "Select top 10 date from Table1 order by date" 
    |> Transpiler.fromString
    |> should equal "
SELECT TOP 10 date
FROM Table1
ORDER BY date "

[<Test>]
let ``Should compile a SELECT statement with a DISTINCT clause as is`` () = 
    "Select Distinct date from Table1" 
    |> Transpiler.fromString
    |> should equal "
SELECT DISTINCT date
FROM Table1"

[<Test>]
let ``Should compile a SELECT statement with many JOINs as is`` () = 
    "select * from Table1 t1 inner join Table2 t2 on t1.id = t2.id left join Table3 on Table3.id >= t2.id3 right join Table4 t4 on t4.id <> t1.id4" 
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table1 t1
INNER JOIN Table2 t2
    ON t1.id = t2.id 
LEFT JOIN Table3
    ON Table3.id >= t2.id3 
RIGHT JOIN Table4 t4
    ON t4.id != t1.id4 "

[<Test>]
let ``Should compile a SELECT statement with right JOIN as is`` () = 
    "select * from Table1 t1 right join Table2 t2 on t1.id = t2.id" 
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table1 t1
RIGHT JOIN Table2 t2
    ON t1.id = t2.id "

[<Test>]
let ``Should compile a SELECT statement with a CASE WHEN as is`` () = 
    "select case id when 1 then 'label1' when 2 then 'label2' else 'nolabel' end" 
    |> Transpiler.fromString
    |> should equal "
SELECT CASE id WHEN 1 THEN 'label1' WHEN 2 THEN 'label2' ELSE 'nolabel' END"

[<Test>]
let ``Should compile a SELECT statement with a CASE WHEN without id as is`` () = 
    "select case when id = 1 then 'label1' when id = 2 then 'label2' else 'nolabel' End"
    |> Transpiler.fromString
    |> should equal "
SELECT CASE WHEN  id = 1  THEN 'label1' WHEN  id = 2  THEN 'label2' ELSE 'nolabel' END"

[<Test>]
let ``Should compile a SELECT statement without tables as is`` () =  
    "select @id = 2 + 2, @text = concat('TEXT','OTHER'), @val = 3 - 0.25, @val2 = 100 * 10, @val4 = 10 / 2.5" 
    |> Transpiler.fromString
    |> should equal "
SELECT @id = 2 + 2, @text = concat('TEXT','OTHER'), @val = 3 - 0.25, @val2 = 100 * 10, @val4 = 10 / 2.5"

[<Test>]
let ``Should compile a SELECT statement with a SUBQUERY using EXISTS operator as is`` () =  
    "select -1, 2.5 as Name, t1.id From Table1 t1 WHERE exists (select top 1 id from Table2 t2 where t2.ext_id = t1.id)" 
    |> Transpiler.fromString
    |> should equal "
SELECT -1, 2.5 AS Name, t1.id
FROM Table1 t1
WHERE EXISTS(
    SELECT TOP 1 id
    FROM Table2 t2
    WHERE t2.ext_id = t1.id ) "

[<Test>]
let ``Should compile a SELECT statement with a WHERE clause using IN operator as is`` () =  
    "select * From Table WHERE role in (1,2,3)" 
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table
WHERE role IN (1, 2, 3) "

[<Test>]
let ``Should compile a SELECT statement with a SUBQUERY using IN operator as is`` () =  
    "select 2.5 as Val, t1.label From Table1 t1 WHERE t1.date in (select distinct date from Table2)" 
    |> Transpiler.fromString
    |> should equal "
SELECT 2.5 AS Val, t1.label
FROM Table1 t1
WHERE t1.date IN  (
    SELECT DISTINCT date
    FROM Table2) "

[<Test>]
let ``Should compile a SELECT statement with a SUBQUERY using EQ operator as is`` () =  
    "select -2.5 as Val, t1.label From Table1 t1 WHERE t1.date = (select top 1 date from Table2)" 
    |> Transpiler.fromString
    |> should equal "
SELECT -2.5 AS Val, t1.label
FROM Table1 t1
WHERE t1.date = (
    SELECT TOP 1 date
    FROM Table2) "


[<Test>]
let ``Should convert a SELECT statement with an OLD SCHOOL *= JOIN to a LEFT JOIN`` () = 
    "select t1.*, t2.* from Table1 t1, Table2 t2 where t1.id *= t2.id" 
    |> Transpiler.fromString
    |> should equal "
SELECT t1.*, t2.*
FROM Table1 t1
LEFT JOIN Table2 t2
    ON t1.id = t2.id "

[<Test>]
let ``Should convert a SELECT statement with an OLD SCHOOL =* JOIN to a RIGHT JOIN`` () = 
    "select t1.*, t2.* from Table1 t1, Table2 t2 where t1.id =* t2.id" 
    |> Transpiler.fromString
    |> should equal "
SELECT t1.*, t2.*
FROM Table1 t1
RIGHT JOIN Table2 t2
    ON t1.id = t2.id "

[<Test>]
let ``Should convert a SELECT statement with an OLD SCHOOL =* JOIN to a RIGHT JOIN and keeping the non concerned tables and where joins`` () = 
    "select t1.*, t2.*, t3.* from Table1 t1, Table2 t2, Table3 t3 where t1.id =* t2.id and t1.id3 = t3.id and (t3.flag = 1 or t3.date = @last)"
    |> Transpiler.fromString
    |> should equal "
SELECT t1.*, t2.*, t3.*
FROM Table1 t1
INNER JOIN Table3 t3
    ON t1.id3 = t3.id 
RIGHT JOIN Table2 t2
    ON t1.id = t2.id 
WHERE(t3.flag = 1 OR t3.date = @last)"

[<Test>]
let ``Should convert a SELECT statement with an OLD SCHOOL =* JOIN to a RIGHT JOIN and keeping the non concerned tables and where joins when outer join is not the first condition`` () = 
    "select t1.*, t2.*, t3.* from Table1 t1, Table2 t2, Table3 t3 where t1.id3 = t3.id and (t3.flag = 1 or t3.date = @last) and t1.id =* t2.id"
    |> Transpiler.fromString
    |> should equal "
SELECT t1.*, t2.*, t3.*
FROM Table1 t1
RIGHT JOIN Table2 t2
    ON t1.id = t2.id 
INNER JOIN Table3 t3
    ON t1.id3 = t3.id 
WHERE(t3.flag = 1 OR t3.date = @last)"

[<Test>]
let ``Should convert a SELECT statement with many OLD SCHOOL =* JOIN to a RIGHT JOIN with many ON clause`` () = 
    "select t1.*, t2.* from Table1 t1, Table2 t2 where t1.id =* t2.id and t1.date =* t2.date" 
    |> Transpiler.fromString
    |> should equal "
SELECT t1.*, t2.*
FROM Table1 t1
RIGHT JOIN Table2 t2
    ON t1.id = t2.id 
        AND t1.date = t2.date "

[<Test>]
let ``Should convert a SELECT statement with many OLD SCHOOL =* and *= JOIN to a CROSS JOIN`` () = 
    "select t1.*, t2.* from Table1 t1, Table2 t2 where t1.id =* t2.id and t1.date *= t2.date" 
    |> Transpiler.fromString
    |> should equal "
SELECT t1.*, t2.*
FROM Table1 t1
CROSS JOIN Table2 t2"

[<Test>]
let ``Should compile an INSERT INTO statement with VALUES as is`` () =  
    "insert into Table(id, label, date) Values (1, 'text', @date)" 
    |> Transpiler.fromString
    |> should equal "
INSERT INTO Table(id, label, date)
VALUES(1, 'text', @date)"

[<Test>]
let ``Should compile an INSERT INTO statement with VALUES as is ans keep indentation`` () =  
    "BEGIN 
        insert into Table(id, label, date) Values (1, 'text', @date)
     END" 
    |> Transpiler.fromString
    |> should equal "
BEGIN
    INSERT INTO Table(id, label, date)
    VALUES(1, 'text', @date)
END"


[<Test>]
let ``Should compile an INSERT statement with VALUES as an INSERT INTO`` () =  
    "insert Table(id, label, date) Values (1, \"\", @date)" 
    |> Transpiler.fromString
    |> should equal "
INSERT INTO Table(id, label, date)
VALUES(1, '', @date)"

[<Test>]
let ``Should compile an INSERT INTO statement with SELECT SUBQUERY as is`` () =  
    "insert into Table(id, label, date) select 1, 'john', date from Table2"
    |> Transpiler.fromString
    |> should equal "
INSERT INTO Table(id, label, date)
    SELECT 1, 'john', date
    FROM Table2"

[<Test>]
let ``Should compile an INSERT INTO statement with a lot of inserts columns separated by lines`` () =  
    "insert into Table(col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col14, col15, col16) 
        select col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col14, col15, col16 from Table2"
    |> Transpiler.fromString
    |> should equal "
INSERT INTO Table(
    col1, col2, col3, col4, col5, col6, col7,
    col8, col9, col10, col11, col12, col14, col15,
    col16)
    SELECT 
        col1, col2, col3, col4, col5, col6, col7,
        col8, col9, col10, col11, col12, col14, col15,
        col16
    FROM Table2"

[<Test>]
let ``Should compile an INSERT INTO statement with a lot of inserts values separated by lines`` () =  
    "insert into Table(col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col14, col15, col16) 
     values(col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col14, col15, col16)"
    |> Transpiler.fromString
    |> should equal "
INSERT INTO Table(
    col1, col2, col3, col4, col5, col6, col7,
    col8, col9, col10, col11, col12, col14, col15,
    col16)
VALUES(
    col1, col2, col3, col4, col5, col6, col7,
    col8, col9, col10, col11, col12, col14, col15,
    col16)"


[<Test>]
let ``Should compile a DELETE statement with a WHERE clause as is`` () =  
    "delete Table where id = 2"
    |> Transpiler.fromString
    |> should equal "
DELETE FROM Table
WHERE id = 2 "

[<Test>]
let ``Should compile a DELETE FROM statement with a WHERE clause as is`` () =  
    "delete from Table where id = 2"
    |> Transpiler.fromString
    |> should equal "
DELETE FROM Table
WHERE id = 2 "

[<Test>]
let ``Should compile a DELETE statement with a JOIN clause as is`` () =  
    "delete t from Table t Inner join Table1 t1 on t.date = t1.date"
    |> Transpiler.fromString
    |> should equal "
DELETE t FROM Table t
INNER JOIN Table1 t1
    ON t.date = t1.date "

[<Test>]
let ``Should compile an UPDATE statement as is`` () =  
    "update Table set date = '99990101', label = 'John' where id = 4"
    |> Transpiler.fromString
    |> should equal "
UPDATE Table
SET 
    date = '99990101',
    label = 'John'
WHERE id = 4 "

[<Test>]
let ``Should compile a massive UPDATE statement as is`` () =  
    "update Table set date = '99990101'"
    |> Transpiler.fromString
    |> should equal "
UPDATE Table
SET date = '99990101'"

[<Test>]
let ``Should compile a DECLARE statement as is`` () =  
    "declare @id int"
    |> Transpiler.fromString
    |> should equal "
DECLARE @id int"

[<Test>]
let ``Should compile a DECLARE statement with a default value as is`` () =  
    "declare @label varchar(3) = 'ABC'"
    |> Transpiler.fromString
    |> should equal "
DECLARE @label varchar(3) = 'ABC'"

[<Test>]
let ``Should compile a DECLARE CURSOR statement as is`` () =  
    "declare myCursor cursor for select id,date from Table"
    |> Transpiler.fromString
    |> should equal "
DECLARE myCursor CURSOR FOR
    SELECT id, date
    FROM Table"

[<Test>]
let ``Should compile a DECLARE CURSOR statement with UPDATE option as is`` () =  
    "declare myCursor cursor for select id,date from Table for update of id, date" 
    |> Transpiler.fromString
    |> should equal "
DECLARE myCursor CURSOR FOR
    SELECT id, date
    FROM Table
FOR UPDATE OF id, date"

[<Test>]
let ``Should compile all CURSOR related statements as is except for the reserved keyword sqlstatus changed to FETCH_STATUS`` () =  
    "begin
       declare myCursor cursor for select id,date from Table for read only
       open myCursor
       while @@sqlstatus <> 1
       begin
         fetch myCursor into @id, @date
         select @id, @date
       end
       close myCursor
       deallocate cursor myCursor
     end" 
    |> Transpiler.fromString
    |> should equal "
BEGIN
    DECLARE myCursor CURSOR FOR
        SELECT id, date
        FROM Table
    FOR READ ONLY

    OPEN myCursor

    WHILE @@FETCH_STATUS != 1 
    BEGIN
        FETCH myCursor INTO @id, @date

        SELECT @id, @date
    END

    CLOSE myCursor

    DEALLOCATE myCursor
END"

[<Test>]
let ``Should compile a DECLARE CURSOR statement with empty UPDATE option as is`` () =  
    "declare myCursor cursor for select id,date from Table for update" 
    |> Transpiler.fromString
    |> should equal "
DECLARE myCursor CURSOR FOR
    SELECT id, date
    FROM Table
FOR UPDATE"

[<Test>]
let ``Should compile a DECLARE CURSOR statement with READ ONLY option as is`` () =  
    "declare myCursor cursor for select id,date from Table for read only" 
    |> Transpiler.fromString
    |> should equal "
DECLARE myCursor CURSOR FOR
    SELECT id, date
    FROM Table
FOR READ ONLY"

[<Test>]
let ``Should compile a RaisError statement into a RAISERROR`` () =  
    "RaisError 99999 'insertion impossible'"
    |> Transpiler.fromString
    |> should equal "
RAISERROR('insertion impossible',1,1)"

[<Test>]
let ``Should compile a IF statement with a single THEN statement as is`` () =  
    "if @id = 1 and @date = @today
        SELECT @flag = 1"
    |> Transpiler.fromString
    |> should equal "
IF @id = 1 
    AND @date = @today 
    SELECT @flag = 1"

[<Test>]
let ``Should compile a IF statement with a single THEN statement and keep parenthesis as is`` () =  
    "if(@id = 1 and (@date < @today or @date is NULL))
        SELECT @flag = 1"
    |> Transpiler.fromString
    |> should equal "
IF(@id = 1 
    AND(@date < @today OR @date IS NULL))
    SELECT @flag = 1"

[<Test>]
let ``Should compile a IF statement with many THEN statements as is`` () =  
    "if @id = 1 and @date = @today
     Begin
        SELECT @flag = 1
        SELECT @flag2 = 4
     End"
    |> Transpiler.fromString
    |> should equal "
IF @id = 1 
    AND @date = @today 
BEGIN
    SELECT @flag = 1

    SELECT @flag2 = 4
END"

[<Test>]
let ``Should compile a IF statement with many THEN statements and a single ELSE statement as is`` () =  
    "if @id > 1 and @label like '%label'
     Begin
        SELECT @flag = 1
        SELECT @flag2 = 4
     End
     ELSE 
        SELECT @flag3 = 1"
    |> Transpiler.fromString
    |> should equal "
IF @id > 1 
    AND @label LIKE '%label' 
BEGIN
    SELECT @flag = 1

    SELECT @flag2 = 4
END
ELSE

    SELECT @flag3 = 1"

[<Test>]
let ``Should compile a IF statement with many THEN statements and many ELSE statements as is`` () =  
    "if @id not in (1,2,3)
     Begin
        SELECT @flag = 1
        SELECT @flag2 = 4
     End
     else 
     BEGIN
        SELECT @flag3 = 1
        SELECT @flag4 = 1
     END"
    |> Transpiler.fromString
    |> should equal "
IF @id NOT IN (1, 2, 3) 
BEGIN
    SELECT @flag = 1

    SELECT @flag2 = 4
END
ELSE

BEGIN
    SELECT @flag3 = 1

    SELECT @flag4 = 1
END"

[<Test>]
let ``Should compile a CALL statement with coma separated values as is`` () =  
    "call(1,0.2,3, 'label')"
    |> Transpiler.fromString
    |> should equal "
call(1,0.2,3,'label')"

[<Test>]
let ``Should compile a CALL to char_length as a call to LEN`` () =  
    "char_length('label')"
    |> Transpiler.fromString
    |> should equal "
LEN('label')"

[<Test>]
let ``Should compile a CALL statement executed with EXEC as is`` () =  
    "exec call(1,0.2,3, 'label')"
    |> Transpiler.fromString
    |> should equal "
EXEC call(1,0.2,3,'label')"
    
[<Test>]
let ``Should compile a CALL statement without parenthesis as is`` () =  
    "exec call 1, @ref output"
    |> Transpiler.fromString
    |> should equal "
EXEC call 1,@ref OUTPUT "

[<Test>]
let ``Should compile a CALL statement without args as is`` () =  
    "call()"
    |> Transpiler.fromString
    |> should equal "
call()"

[<Test>]
let ``Should compile a LABEL statement as is`` () =  
    "label:"
    |> Transpiler.fromString
    |> should equal "

label: 
"

[<Test>]
let ``Should compile a GOTO statement as is`` () =  
    "goto label"
    |> Transpiler.fromString
    |> should equal "
GOTO label"

[<Test>]
let ``Should compile a RETURN statement as is`` () =  
    "return"
    |> Transpiler.fromString
    |> should equal "
RETURN"

[<Test>]
let ``Should compile a RETURN statement with value as is`` () =  
    "return -2.5"
    |> Transpiler.fromString
    |> should equal "
RETURN -2.5"

[<Test>]
let ``Should compile a STORED PROCEDURE without args creation script as is`` () =  
    "CREATE PROC Name 
    AS
    BEGIN
       RETURN -1
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name
AS

BEGIN
    RETURN -1
END
GO
"

[<Test>]
let ``Should compile a STORED PROCEDURE with args creation script as is`` () =  
    "CREATE PROC Name(arg1 int, arg2 char(10), arg3 char(1) = 'A', arg4 float output)
    AS
    BEGIN
       RETURN -1
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name(
    arg1 int,
    arg2 char(10),
    arg3 char(1) = 'A',
    arg4 float OUTPUT
)
AS

BEGIN
    RETURN -1
END
GO
"

[<Test>]
let ``Should compile a STORED PROCEDURE with args creation with default values script as is`` () =  
    "CREATE PROC Name(@integer int = 1, @negInteger int = -1, @float decimal = 1.2, @negFloat decimal = -4.55, @text varchar(100) = 'TEXT', @nullable PLOP = NULL)
    AS
    BEGIN
       RETURN -1
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name(
    @integer int = 1,
    @negInteger int = -1,
    @float decimal = 1.2,
    @negFloat decimal = -4.55,
    @text varchar(100) = 'TEXT',
    @nullable PLOP = NULL
)
AS

BEGIN
    RETURN -1
END
GO
"


[<Test>]
let ``Should compile a STORED PROCEDURE with nested begin end statement bloc`` () =  
    "CREATE PROC Name
    AS
    BEGIN
       BEGIN 
        DECLARE @toto INT
        DECLARE @titi CHAR
        BEGIN 
            SELECT * FROM Table
            UPDATE Table SET Col = @toto
        END
       END
       RETURN -2
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name
AS

BEGIN
    BEGIN
        DECLARE @toto INT

        DECLARE @titi CHAR

        BEGIN
            SELECT *
            FROM Table

            UPDATE Table
            SET Col = @toto
        END
    END

    RETURN -2
END
GO
"

[<Test>]
let ``Should compile a STORED PROCEDURE with args and without parenthesis creation script as is`` () =  
    "CREATE PROC Name 
        arg1 int, 
        arg2 char(10), 
        arg3 char(1) = 'A', 
        arg4 float = 0.1
    AS
    BEGIN
       RETURN -1
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name 
    arg1 int,
    arg2 char(10),
    arg3 char(1) = 'A',
    arg4 float = 0.1
 
AS

BEGIN
    RETURN -1
END
GO
"


[<Test>]
let ``Should compile a STORED PROCEDURE creation but limiting to 8000 the varchar size`` () =  
    "CREATE PROC Name(arg1 varchar(20000))
    AS
    BEGIN
       RETURN -1
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name(
    arg1 varchar(8000)
)
AS

BEGIN
    RETURN -1
END
GO
"

[<Test>]
let ``Should compile a STORED PROCEDURE creation but keeping the special options`` () =  
    "CREATE PROC Name(arg1 int)
    WITH recompile AS
    BEGIN
       RETURN -1
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name(
    arg1 int
)
WITH recompile
AS

BEGIN
    RETURN -1
END
GO
"

[<Test>]
let ``Should compile a STORED PROCEDURE with a body containing many statement script as is`` () =  
    "CREATE PROC Name( arg1 int, arg2 numeric(10,1), arg3 char(1) = 'A', arg4 float output )
    AS
    BEGIN
       DECLARE @date DATETIME = '99990101'
       exec OtherProc (1, 2, 3, @date output)
       RETURN -1
    END"
    |> Transpiler.fromString
    |> should equal "CREATE PROC Name(
    arg1 int,
    arg2 numeric(10,1),
    arg3 char(1) = 'A',
    arg4 float OUTPUT
)
AS

BEGIN
    DECLARE @date DATETIME = '99990101'

    EXEC OtherProc(1,2,3,@date OUTPUT)

    RETURN -1
END
GO
"

[<Test>]
let ``Should compile a WHILE statement as is`` () =  
    "while 1 = 1
    begin 
        declare @id int = 1
        call(@id,2,'label')
    end"
    |> Transpiler.fromString
    |> should equal "
WHILE 1 = 1 
BEGIN
    DECLARE @id int = 1

    call(@id,2,'label')
END"

[<Test>]
let ``Should rewrite a SELECT statement having many invalid left or right joins but keeping the right dependencies order between tables`` () =  
    "SELECT t1.*
    FROM Table1 t1, Table2 t2, Table3 t3, Table4 t4
    WHERE t1.id = 1
    AND t1.id2 *= t2.id
    AND t1.date = '20200101'
    AND    t3.id1 =* t1.id
    AND t3.date1 =* t1.date
    AND t1.id4 = t4.id"
    |> Transpiler.fromString
    |> should equal "
SELECT t1.*
FROM Table1 t1
INNER JOIN Table4 t4
    ON t1.id4 = t4.id 
RIGHT JOIN Table3 t3
    ON t3.id1 = t1.id 
        AND t3.date1 = t1.date 
LEFT JOIN Table2 t2
    ON t1.id2 = t2.id 
WHERE t1.id = 1 
    AND t1.date = '20200101' "

[<Test>]
let ``Should rewrite a SELECT statement having many joins between all the tables`` () =  
    "SELECT *
    FROM Table1 t1, Table2 t2, Table3 t3
    WHERE t2.ind = t3.ind
    AND t2.cat = 'C'
    AND t3.val = 'A'
    AND t1.date = t2.date
    AND    t1.id = t2.id
    AND t1.id3 = t3.id"
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table2 t2
INNER JOIN Table1 t1
    ON t1.date = t2.date 
        AND t1.id = t2.id 
INNER JOIN Table3 t3
    ON t2.ind = t3.ind 
        AND t1.id3 = t3.id 
WHERE t2.cat = 'C' 
    AND t3.val = 'A' "

[<Test>]
let ``Should rewrite a SELECT statement keeping every root table`` () =  
    "SELECT *
    FROM Table1 t1, Table2 t2, Table3 t3, Table4 t4
    WHERE t2.cat = 'C'
    AND t3.val = 'A'
    AND t1.date = t2.date
    AND    t1.id = t2.id
    AND t3.id4 = t4.id"
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table3 t3, Table1 t1
INNER JOIN Table4 t4
    ON t3.id4 = t4.id 
INNER JOIN Table2 t2
    ON t1.date = t2.date 
        AND t1.id = t2.id 
WHERE t2.cat = 'C' 
    AND t3.val = 'A' "

[<Test>]
let ``Should rewrite a SELECT statement keeping existing joins with the right dependency order`` () =  
    "SELECT *
    FROM Table1 t1, Table2 t2
    INNER JOIN Table3 t3
        ON t1.id3 = t3.id
    LEFT JOIN Table4 t4
        ON t3.id4 = t4.id
    WHERE t2.cat = 'C'
    AND t3.val = 'A'
    AND t1.date = t2.date
    AND    t1.id = t2.id"
    |> Transpiler.fromString
    |> should equal "
SELECT *
FROM Table1 t1
INNER JOIN Table2 t2
    ON t1.date = t2.date 
        AND t1.id = t2.id 
INNER JOIN Table3 t3
    ON t1.id3 = t3.id 
LEFT JOIN Table4 t4
    ON t3.id4 = t4.id 
WHERE t2.cat = 'C' 
    AND t3.val = 'A' "